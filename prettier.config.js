module.exports = {
    singleQuote: true,
    trailingComma: 'none',
    tabs: true,
    tabWidth: 4,
    printWidth: 120
};
