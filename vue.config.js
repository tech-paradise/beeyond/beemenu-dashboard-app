module.exports = {
    transpileDependencies: ['vuetify'],
    configureWebpack: {
        devtool: 'source-map'
    },
    chainWebpack: config => {
        config.plugin('html').tap(args => {
            args[0].title = 'BeeMenu - Panel de Administración';
            return args;
        });
    }
};
