import { Action, Module, VuexModule } from 'vuex-module-decorators';
import { firestoreAction } from 'vuexfire';
import { db } from '@/config/firebase';
import { MENUS } from '@/config/collectionsName';
import { serializeMenu, newTimestamp } from '@/config/serializables';
import { Menu } from '@/model/menu/Menu';

@Module({
    namespaced: true
})
export class MenuModule extends VuexModule {
    menu: Menu = new Menu('', '', '', '', '', '', '', '', '', false, false, false, newTimestamp(), false, false);

    @Action({ rawError: true })
    async findMenuById(menuId: string) {
        const action = firestoreAction(({ bindFirestoreRef }, menId: string) => {
            return bindFirestoreRef('menu', db.collection(MENUS).doc(menId), {
                serialize: serializeMenu,
                reset: false
            });
        }) as Function;
        await action(this.context, menuId);
    }
}
