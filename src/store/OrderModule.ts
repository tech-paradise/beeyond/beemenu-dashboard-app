import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { db } from '@/config/firebase';
import { ORDERS } from '@/config/collectionsName';
import { Order } from '@/model/order/Order';
import { firestoreAction } from 'vuexfire';
import { serializeOrder } from '@/config/serializables';
import { OrderService } from '@/services/OrderService';

@Module({ namespaced: true })
export class OrderModule extends VuexModule {
    private readonly orderService = new OrderService();
    orders: Order[] = [];

    @Mutation
    setOrders(payload: Order[]) {
        this.orders = payload;
    }

    @Action({ rawError: true })
    async findOrdersForRestaurant(restaurantId: string) {
        const action = firestoreAction(({ bindFirestoreRef }, resId) => {
            return bindFirestoreRef(
                'orders',
                db
                    .collection(ORDERS)
                    .where('restaurantId', '==', resId)
                    .orderBy('createdAt', 'desc'),
                {
                    serialize: serializeOrder
                }
            );
        }) as Function;

        await action(this.context, restaurantId);
    }

    @Action({ rawError: true })
    async changeStatusOrder(payload: { orderId: string; status: string }) {
        await this.orderService.updateStatusOrder(payload.orderId, payload.status);
    }
}
