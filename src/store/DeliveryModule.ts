import { Action, Module, VuexModule } from 'vuex-module-decorators';
import { firestoreAction } from 'vuexfire';
import { db } from '@/config/firebase';
import { DELIVERIES } from '@/config/collectionsName';
import { serializeDelivery } from '@/config/serializables';
import { DeliveryMenu } from '@/model/delivery/DeliveryMenu';
import { DEFAULT_SCHEDULE } from '@/model/delivery/Schedule';
import { DEFAULT_COST_SHIPPING } from '@/model/delivery/CostShipping';
import { DEFAULT_PHONES } from '@/model/delivery/Phones';
import { DEFAULT_PAYMENT } from '@/model/delivery/Payment';
import { DEFAULT_LOCATION } from '@/model/delivery/Location';

@Module({
    namespaced: true
})
export class DeliveryModule extends VuexModule {
    delivery: DeliveryMenu = new DeliveryMenu(
        '',
        '',
        DEFAULT_COST_SHIPPING,
        '',
        DEFAULT_PAYMENT,
        DEFAULT_PHONES,
        '',
        DEFAULT_SCHEDULE,
        '',
        false,
        false,
        '',
        DEFAULT_LOCATION,
        false,
        false
    );

    @Action({ rawError: true })
    async findDeliveryByMenuId(menuId: string) {
        const action = firestoreAction(({ bindFirestoreRef }, menuId: string) => {
            return bindFirestoreRef('delivery', db.collection(DELIVERIES).doc(menuId), {
                serialize: serializeDelivery,
                reset: false
            });
        }) as Function;
        await action(this.context, menuId);
    }
}
