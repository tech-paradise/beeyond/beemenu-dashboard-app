import { Action, Module, VuexModule } from 'vuex-module-decorators';
import { firestoreAction } from 'vuexfire';
import { db } from '@/config/firebase';
import { serializeSocialNetworks } from '@/config/serializables';
import { SOCIALNETWORKS } from '@/config/collectionsName';
import SocialNetworks from '@/model/socialNetworks/SocialNetworks';

@Module({
    namespaced: true
})
export class SocialNetworksModule extends VuexModule {
    socialNetworks: SocialNetworks = new SocialNetworks(
        '',
        '',
        '',
        '',
        ''
    );

    @Action({ rawError: true })
    async findSocialNetworksByMenuId(menuId: string) {
        const action = firestoreAction(({ bindFirestoreRef }, menuId: string) => {
            return bindFirestoreRef('socialNetworks', db.collection(SOCIALNETWORKS).doc(menuId), {
                serialize: serializeSocialNetworks,
                reset: false
            });
        }) as Function;
        await action(this.context, menuId);
    }
}
