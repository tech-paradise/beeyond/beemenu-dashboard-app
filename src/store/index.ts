import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersistence from 'vuex-persist';
import { LoginModule } from '@/store/LoginModule';
import { RestaurantModule } from '@/store/RestaurantModule';
import { OrderModule } from '@/store/OrderModule';
import { vuexfireMutations } from 'vuexfire';
import { MenuModule } from '@/store/MenuModule';
import { User } from '@/model/User';
import { KpiModule } from '@/store/KpiModule';
import { DeliveryModule } from '@/store/DeliveryModule';
import { SocialNetworksModule } from '@/store/SocialNetworksModule';

Vue.use(Vuex);

export interface State {
    [key: string]: any;
}

const vuexLocal = new VuexPersistence({
    storage: window.localStorage
});

const store = new Vuex.Store({
    state: {
        showHomeIcon: true,
        isLoading: false
    } as State,
    mutations: {
        setShowHomeIcon(state, payload: boolean) {
            state.showHomeIcon = payload;
        },
        setLoading(state, payload: boolean) {
            state.isLoading = payload;
        },
        ...vuexfireMutations
    },
    actions: {
        async changeShowHomeIcon(context, payload: boolean) {
            context.commit('setShowHomeIcon', payload);
        },
        async changeLoading(context, payload: boolean) {
            context.commit('setLoading', payload);
        }
    },
    modules: {
        LoginModule,
        RestaurantModule,
        OrderModule,
        MenuModule,
        KpiModule,
        DeliveryModule,
        SocialNetworksModule
    },
    plugins: [vuexLocal.plugin]
});

store.watch(
    (state: State) => state.LoginModule.user as User,
    async newUser => {
        if (newUser) {
            await store.dispatch('RestaurantModule/findRestaurants');
        }
    }
);

export default store;
