import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { auth, db } from '@/config/firebase';
import { User } from '@/model/User';
import { firestoreAction } from 'vuexfire';
import { USER } from '@/config/collectionsName';
import { mainSerialize } from '@/config/serializables';
import router from '@/router';

@Module({ namespaced: true })
export class LoginModule extends VuexModule {
    user: User = new User('', '', '', [], []);
    error = '';
    @Mutation
    setUser(payload: User) {
        this.user = payload;
    }

    @Mutation
    setError(payload: string) {
        this.error = payload;
    }

    @Action({ rawError: true })
    async login(payload: { email: string; pass: string }): Promise<void> {
        try {
            await auth.signInWithEmailAndPassword(payload.email, payload.pass);
        } catch (e) {
            this.context.commit('setError', e);
        }
        await this.context.dispatch('changeLoading', false, { root: true });
    }

    @Action({ rawError: true })
    async logout(): Promise<void> {
        try {
            await auth.signOut();
            this.context.commit('setUser', new User('', '', '', [], []));
            this.context.commit('setError', '');
            await this.context.dispatch('RestaurantModule/clean', null, { root: true });
            await router.push('/login');
        } catch (e) {
            this.context.commit('setError', e.message);
        }
    }

    @Action({ rawError: true })
    async incomingUser(payload: string) {

        const action = firestoreAction(async ({ bindFirestoreRef }, uidUser: string) => {
            await bindFirestoreRef('user', db.collection(USER).doc(uidUser), {
                serialize: mainSerialize
            });
        }) as Function;

        await action(this.context, payload);
        if (router.currentRoute.path === '/login') {
            await router.push('/admin');
        }
    }

    @Action
    incomingError(payload: string) {
        this.context.commit('setError', payload);
    }
}
