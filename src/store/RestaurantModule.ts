import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { Restaurant } from '@/model/Restaurant';
import { User } from '@/model/User';
import { RESTAURANTS } from '@/config/collectionsName';
import { db } from '@/config/firebase';
import { firestoreAction } from 'vuexfire';
import { serializeRestaurant } from '@/config/serializables';

@Module({ namespaced: true })
export class RestaurantModule extends VuexModule {
    restaurants: Restaurant[] = [];

    @Mutation
    setRestaurants(payload: Restaurant[]) {
        this.restaurants = payload;
    }

    @Action({ rawError: true })
    async findRestaurants() {
        const user: User = this.context.rootState.LoginModule.user;

        if (user.rols.includes('BEE')) {
            const action = firestoreAction(async ({ bindFirestoreRef }) => {
                await bindFirestoreRef(RESTAURANTS, db.collection(RESTAURANTS), { serialize: serializeRestaurant });
                return;
            }) as Function;
            await action(this.context);
            return;
        }

        if (user.rols.includes('ADMIN') || user.rols.includes('SUPERADMIN')) {
            this.context.commit('setRestaurants', user.restaurants);
        }
    }

    @Action({ rawError: true })
    clean() {
        const action = firestoreAction(({ unbindFirestoreRef }) => {
            return unbindFirestoreRef('restaurants');
        }) as Function;

        return action(this.context);
    }
}
