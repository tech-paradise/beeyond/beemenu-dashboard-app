import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import { KPIService } from '@/services/KPIService';
import { RecordMenuViews, ViewType } from '@/model/kpi/View';
import { DataRange } from '@/model/DataRange';
import { TypeOrder } from '@/model/order/Order';
import { FilterOrder } from '@/model/kpi/KpiFilterOrder';
import { dateToString } from '@/StringUtils';

@Module({ namespaced: true })
export class KpiModule extends VuexModule {
    menuHead: { DELIVERY: boolean; COLLECT: boolean; LOCAL: boolean } = {
        DELIVERY: false,
        COLLECT: false,
        LOCAL: false
    };
    filterMenuHead: { DAY: boolean; WEEK: boolean; MONTH: boolean } = { DAY: false, WEEK: false, MONTH: false };
    stateMenu = '';
    stateFilter = '';
    recordMenuViewsMonth: RecordMenuViews[] = [];
    recordMenuViewsWeek: RecordMenuViews[] = [];
    recordMenuViewsPerDay: RecordMenuViews[] = [];

    recordMenuViewsDeliveryPerDay: RecordMenuViews[] = [];
    recordMenuViewsDeliveryWeek: RecordMenuViews[] = [];
    recordMenuViewsDeliveryMonth: RecordMenuViews[] = [];

    recordMenuViewsCollectPerDay: RecordMenuViews[] = [];
    recordMenuViewsCollectWeek: RecordMenuViews[] = [];
    recordMenuViewsCollectMonth: RecordMenuViews[] = [];

    recordMenuViewsLocal: RecordMenuViews[] = [];

    kpiService = new KPIService();

    //SET MUTATIONS
    @Mutation
    setRecordMenuViewsWeek(payload: RecordMenuViews[]) {
        this.recordMenuViewsWeek = payload;
    }
    @Mutation
    setRecordMenuViewsDeliveryWeek(payload: RecordMenuViews[]) {
        this.recordMenuViewsDeliveryWeek = payload;
    }
    @Mutation
    setRecordMenuViewsCollectWeek(payload: RecordMenuViews[]) {
        this.recordMenuViewsCollectWeek = payload;
    }

    @Mutation
    setRecordMenuViewsMonth(payload: RecordMenuViews[]) {
        this.recordMenuViewsMonth = payload;
    }
    @Mutation
    setRecordMenuViewsDeliveryMonth(payload: RecordMenuViews[]) {
        this.recordMenuViewsDeliveryMonth = payload;
    }
    @Mutation
    setRecordMenuViewsCollectMonth(payload: RecordMenuViews[]) {
        this.recordMenuViewsCollectMonth = payload;
    }

    @Mutation
    setRecordMenuViewsPerDay(payload: RecordMenuViews[]) {
        this.recordMenuViewsPerDay = payload;
    }
    @Mutation
    setRecordMenuViewsDeliveryPerDay(payload: RecordMenuViews[]) {
        this.recordMenuViewsDeliveryPerDay = payload;
    }
    @Mutation
    setRecordMenuViewsCollectPerDay(payload: RecordMenuViews[]) {
        this.recordMenuViewsCollectPerDay = payload;
    }

    @Mutation
    setMenuHead(payload: { DELIVERY: boolean; COLLECT: boolean; LOCAL: boolean }) {
        this.menuHead = payload;
    }

    @Mutation
    setFilterMenuHead(payload: { DAY: boolean; WEEK: boolean; MONTH: boolean }) {
        this.filterMenuHead = payload;
    }

    @Mutation
    setStateMenuOpc(payload: string) {
        this.stateMenu = payload;
    }

    @Mutation
    setStateFilterOpc(payload: string) {
        this.stateFilter = payload;
    }

    //+ADDED
    @Mutation
    setRecordMenuViewsLocal(payload: RecordMenuViews[]) {
        this.recordMenuViewsLocal = payload;
    }

    //  ADD MUTATIONS
    @Mutation
    addDataMenuViewWeek(payload: RecordMenuViews) {
        this.recordMenuViewsWeek.push(payload);
    }
    @Mutation
    addDataMenuViewDeliveryWeek(payload: RecordMenuViews) {
        this.recordMenuViewsDeliveryWeek.push(payload);
    }
    @Mutation
    addDataMenuViewCollectWeek(payload: RecordMenuViews) {
        this.recordMenuViewsCollectWeek.push(payload);
    }

    @Mutation
    addDataMenuViewMonth(payload: RecordMenuViews) {
        this.recordMenuViewsMonth.push(payload);
    }
    @Mutation
    addDataMenuViewDeliveryMonth(payload: RecordMenuViews) {
        this.recordMenuViewsDeliveryMonth.push(payload);
    }
    @Mutation
    addDataMenuViewCollectMonth(payload: RecordMenuViews) {
        this.recordMenuViewsCollectMonth.push(payload);
    }

    @Mutation
    addDataMenuViewPerDay(payload: RecordMenuViews) {
        this.recordMenuViewsPerDay.push(payload);
    }
    @Mutation
    addDataMenuViewDeliveryPerDay(payload: RecordMenuViews) {
        this.recordMenuViewsDeliveryPerDay.push(payload);
    }
    @Mutation
    addDataMenuViewCollectPerDay(payload: RecordMenuViews) {
        this.recordMenuViewsCollectPerDay.push(payload);
    }

    //+ADDED
    @Mutation
    addDataMenuViewLocal(payload: RecordMenuViews) {
        this.recordMenuViewsLocal.push(payload);
    }

    @Action({ rawError: true })
    async getViewsForMenu(payload: { menuId: string }) {
        await this.context.dispatch('getMenusViewsMonthly', payload);
        await this.context.dispatch('getMenusViewsWeekly', payload);
        await this.context.dispatch('getMenusViewsPerDay', payload);

        await this.context.dispatch('getMenusViewsDeliveryPerDay', payload);
        await this.context.dispatch('getMenusViewsDeliveryWeekly', payload);
        await this.context.dispatch('getMenusViewsDeliveryMonthly', payload);

        await this.context.dispatch('getMenusViewsCollectPerDay', payload);
        await this.context.dispatch('getMenusViewsCollectWeekly', payload);
        await this.context.dispatch('getMenusViewsCollectMonthly', payload);

        //+ADDED
        await this.context.dispatch('getMenusViewsLocal', payload);
    }

    @Action({ rawError: true })
    async setStateMenu(payload: { menuOpc: string }) {
        this.context.commit('setStateMenuOpc', payload.menuOpc);
    }
    @Action({ rawError: true })
    async setStateFilter(payload: { stateFilter: string }) {
        this.context.commit('setStateFilterOpc', payload.stateFilter);
    }

    @Action({ rawError: true })
    async setOptionMenu(payload: { optionMenu: TypeOrder }) {
        this.context.commit('setMenuHead', { DELIVERY: false, COLLECT: false, LOCAL: false });

        switch (payload.optionMenu) {
            case 'DELIVERY':
                this.context.commit('setMenuHead', { DELIVERY: true, COLLECT: false, LOCAL: false });
                break;
            case 'COLLECT':
                this.context.commit('setMenuHead', { DELIVERY: false, COLLECT: true, LOCAL: false });
                break;
            case 'LOCAL':
                this.context.commit('setMenuHead', { DELIVERY: false, COLLECT: false, LOCAL: true });
                break;
            default:
                this.context.commit('setMenuHead', { DELIVERY: false, COLLECT: false, LOCAL: false });
                break;
        }
    }
    @Action({ rawError: true })
    async setFilterOption(payload: { filterOptions: FilterOrder }) {
        this.context.commit('setFilterMenuHead', { DELIVERY: false, COLLECT: false, LOCAL: false });

        switch (payload.filterOptions) {
            case 'DAY':
                this.context.commit('setFilterMenuHead', { DAY: true, WEEK: false, MONTH: false });
                break;
            case 'WEEK':
                this.context.commit('setFilterMenuHead', { DAY: false, WEEK: true, MONTH: false });
                break;
            case 'MONTH':
                this.context.commit('setFilterMenuHead', { DAY: false, WEEK: false, MONTH: true });
                break;
            default:
                this.context.commit('setFilterMenuHead', { DAY: false, WEEK: false, MONTH: false });
                break;
        }
    }

    @Action({ rawError: true })
    async getMenusViewsLocal(payload: { menuId: string }) {
        this.context.commit('setRecordMenuViewsLocal', []);
        //Limite es el mes actual
        const date = new Date();
        const mesActual = date.getMonth();
        const diaActual = date.getDate();
        date.setFullYear(date.getFullYear(), mesActual, diaActual);
        date.setHours(0, 0, 0);
        const menuLocalViews = await this.kpiService.getMenusViewsType(payload.menuId, mesActual);

        console.log('STORE VIEWS ', menuLocalViews);
        for (let dayOfMonth = 1; dayOfMonth <= DataRange.MONTHLY * 2; dayOfMonth++) {
            console.log('TIME FOR', dayOfMonth, DataRange.MONTHLY * 2, dayOfMonth);
            const monthViews = menuLocalViews.filter(m => {
                m.createdAt.getDate() === dayOfMonth;

            }).length;

            const date = new Date();
            date.setFullYear(date.getFullYear(), date.getMonth() - 1, dayOfMonth);
            date.setHours(0, 0, 0);
            this.context.commit('addDataMenuViewLocal', new RecordMenuViews(new Date(dateToString(date)), monthViews));
        }
    }

    @Action({ rawError: true })
    async getMenusViewsWeekly(payload: { menuId: string }) {
        this.context.commit('setRecordMenuViewsWeek', []);

        //Limite es el mes actual
        const date = new Date();
        const mesActual = date.getMonth();
        const diaActual = date.getDate();
        date.setFullYear(date.getFullYear(), mesActual, diaActual);
        date.setHours(0, 0, 0);

        const menuWeekViews = await this.kpiService.getMenusViewsByType(
            payload.menuId,
            mesActual,
            DataRange.WEEKLY,
            ViewType.LOCAL
        );
        const startDate = new Date();
        const endDate = new Date();

        for (
            let numberOfDay = startDate.getDate() - DataRange.WEEKLY;
            numberOfDay <= Number(endDate.getDate());
            numberOfDay++
        ) {
            if (Number(startDate.getDate() - DataRange.WEEKLY) < 0) {
                let weeks = 0;
                if (numberOfDay <= 0) {
                    weeks = menuWeekViews.filter(
                        m => m.createdAt.getDate() == Number(numberOfDay + DataRange.MONTHLY + 1)
                    ).length;
                } else {
                    weeks = menuWeekViews.filter(m => m.createdAt.getDate() == numberOfDay).length;
                }

                const date = new Date();
                date.setFullYear(date.getFullYear(), date.getMonth(), numberOfDay);
                date.setHours(0, 0, 0);
                this.context.commit('addDataMenuViewWeek', new RecordMenuViews(new Date(dateToString(date)), weeks));
            } else {
                const weeksViewsByDay = menuWeekViews.filter(m => m.createdAt.getDate() == numberOfDay).length;
                const date = new Date();
                date.setFullYear(date.getFullYear(), date.getMonth(), numberOfDay);
                this.context.commit(
                    'addDataMenuViewWeek',
                    new RecordMenuViews(new Date(dateToString(date)), weeksViewsByDay)
                );
            }
        }
    }

    @Action({ rawError: true })
    async getMenusViewsMonthly(payload: { menuId: string }) {
        this.context.commit('setRecordMenuViewsMonth', []);

        //Limite es el mes actual
        const date = new Date();
        const mesActual = date.getMonth();
        const diaActual = date.getDate();

        date.setFullYear(date.getFullYear(), mesActual, diaActual);
        date.setHours(0, 0, 0);

        const menuViewsMonthly = await this.kpiService.getMenusViewsByType(
            payload.menuId,
            mesActual,
            DataRange.MONTHLY,
            ViewType.LOCAL
        );
        //Recorre array y lo pasa al constructor RecordMenuViews
        for (let dayOfMonth = 1; dayOfMonth <= DataRange.MONTHLY + 1; dayOfMonth++) {
            const monthViews = menuViewsMonthly.filter(m => m.createdAt.getDate() == dayOfMonth).length;

            const date = new Date();
            date.setFullYear(date.getFullYear(), date.getMonth() - 1, dayOfMonth);
            date.setHours(0, 0, 0);

            this.context.commit('addDataMenuViewMonth', new RecordMenuViews(new Date(dateToString(date)), monthViews));
        }
    }

    @Action({ rawError: true })
    async getMenusViewsPerDay(payload: { menuId: string }) {
        this.context.commit('setRecordMenuViewsPerDay', []);
        //Limite es el mes actual
        const date = new Date();
        const mesActual = date.getMonth();
        const diaActual = date.getDate();

        date.setFullYear(date.getFullYear(), mesActual, diaActual);
        date.setHours(0, 0, 0);

        const menuViewsMonthly = await this.kpiService.getMenusViewsByType(
            payload.menuId,
            mesActual,
            DataRange.DAILY,
            ViewType.LOCAL
        );
        for (let numberMonth = date.getDate() - 1; numberMonth <= date.getDate() - 1; numberMonth++) {
            const viewsByMonth = menuViewsMonthly.filter(m => m.createdAt.getDate() == numberMonth).length;
            const date = new Date();
            date.setFullYear(date.getFullYear(), date.getMonth(), numberMonth);
            this.context.commit(
                'addDataMenuViewPerDay',
                new RecordMenuViews(new Date(dateToString(date)), viewsByMonth)
            );
        }
    }

    @Action({ rawError: true })
    async getMenusViewsDeliveryWeekly(payload: { menuId: string }) {
        this.context.commit('setRecordMenuViewsDeliveryWeek', []);
        //Limite es el mes actual
        const date = new Date();
        const mesActual = date.getMonth();
        const diaActual = date.getDate();

        date.setFullYear(date.getFullYear(), mesActual, diaActual);
        date.setHours(0, 0, 0);

        const menuWeekViews = await this.kpiService.getMenusViewsByType(
            payload.menuId,
            mesActual,
            DataRange.WEEKLY,
            ViewType.DELIVERY
        );
        const startDate = new Date();
        const endDate = new Date();

        for (
            let numberOfDay = startDate.getDate() - DataRange.WEEKLY;
            numberOfDay <= Number(endDate.getDate());
            numberOfDay++
        ) {
            if (Number(startDate.getDate() - DataRange.WEEKLY) < 0) {
                let weeks = 0;
                if (numberOfDay <= 0) {
                    weeks = menuWeekViews.filter(
                        m => m.createdAt.getDate() == Number(numberOfDay + DataRange.MONTHLY + 1)
                    ).length;
                } else {
                    weeks = menuWeekViews.filter(m => m.createdAt.getDate() == numberOfDay).length;
                }
                const date = new Date();
                date.setFullYear(date.getFullYear(), date.getMonth(), numberOfDay);
                date.setHours(0, 0, 0);
                this.context.commit(
                    'addDataMenuViewDeliveryWeek',
                    new RecordMenuViews(new Date(dateToString(date)), weeks)
                );
            } else {
                const weeksViewsByDay = menuWeekViews.filter(m => m.createdAt.getDate() == numberOfDay).length;
                const date = new Date();
                date.setFullYear(date.getFullYear(), date.getMonth(), numberOfDay);
                this.context.commit(
                    'addDataMenuViewDeliveryWeek',
                    new RecordMenuViews(new Date(dateToString(date)), weeksViewsByDay)
                );
            }
        }
    }

    @Action({ rawError: true })
    async getMenusViewsDeliveryMonthly(payload: { menuId: string }) {
        this.context.commit('setRecordMenuViewsDeliveryMonth', []);
        //Limite es el mes actual
        const date = new Date();
        const mesActual = date.getMonth();
        const diaActual = date.getDate();
        date.setFullYear(date.getFullYear(), mesActual, diaActual);
        date.setHours(0, 0, 0);

        const menuViewsMonthly = await this.kpiService.getMenusViewsByType(
            payload.menuId,
            mesActual,
            DataRange.MONTHLY,
            ViewType.DELIVERY
        );

        //Recorre array y lo pasa al constructor RecordMenuViews
        for (let dayOfMonth = 1; dayOfMonth <= DataRange.MONTHLY + 1; dayOfMonth++) {
            const monthViews = menuViewsMonthly.filter(m => m.createdAt.getDate() == dayOfMonth).length;

            const date = new Date();
            date.setFullYear(date.getFullYear(), date.getMonth() - 1, dayOfMonth);
            date.setHours(0, 0, 0);
            const formattedDate = date
                .toLocaleDateString('en-GB', {
                    day: 'numeric',
                    month: 'short',
                    year: 'numeric'
                })
                .replace(/ /g, '-');
            this.context.commit(
                'addDataMenuViewDeliveryMonth',
                new RecordMenuViews(new Date(formattedDate), monthViews)
            );
        }
    }

    @Action({ rawError: true })
    async getMenusViewsDeliveryPerDay(payload: { menuId: string }) {
        this.context.commit('setRecordMenuViewsDeliveryPerDay', []);
        //Limite es el mes actual
        const date = new Date();
        const mesActual = date.getMonth();
        const diaActual = date.getDate();

        date.setFullYear(date.getFullYear(), mesActual, diaActual);
        date.setHours(0, 0, 0);

        const menuViewsMonthly = await this.kpiService.getMenusViewsByType(
            payload.menuId,
            mesActual,
            DataRange.DAILY,
            ViewType.DELIVERY
        );

        for (let numberMonth = date.getDate() - 1; numberMonth <= date.getDate() - 1; numberMonth++) {
            const viewsByMonth = menuViewsMonthly.filter(m => m.createdAt.getDate() == numberMonth).length;

            const date = new Date();
            date.setFullYear(date.getFullYear(), date.getMonth(), numberMonth);
            this.context.commit(
                'addDataMenuViewDeliveryPerDay',
                new RecordMenuViews(new Date(dateToString(date)), viewsByMonth)
            );
        }
    }

    @Action({ rawError: true })
    async getMenusViewsCollectWeekly(payload: { menuId: string }) {
        this.context.commit('setRecordMenuViewsCollectWeek', []);
        //Limite es el mes actual
        const date = new Date();
        const mesActual = date.getMonth();
        const diaActual = date.getDate();

        date.setFullYear(date.getFullYear(), mesActual, diaActual);
        date.setHours(0, 0, 0);

        const menuWeekViews = await this.kpiService.getMenusViewsByType(
            payload.menuId,
            mesActual,
            DataRange.WEEKLY,
            ViewType.COLLECT
        );
        const startDate = new Date();
        const endDate = new Date();

        for (
            let numberOfDay = startDate.getDate() - DataRange.WEEKLY;
            numberOfDay <= Number(endDate.getDate());
            numberOfDay++
        ) {
            if (Number(startDate.getDate() - DataRange.WEEKLY) < 0) {
                let weeks = 0;
                if (numberOfDay <= 0) {
                    weeks = menuWeekViews.filter(
                        m => m.createdAt.getDate() == Number(numberOfDay + DataRange.MONTHLY + 1)
                    ).length;
                } else {
                    weeks = menuWeekViews.filter(m => m.createdAt.getDate() == numberOfDay).length;
                }

                const date = new Date();
                date.setFullYear(date.getFullYear(), date.getMonth(), numberOfDay);
                date.setHours(0, 0, 0);
                const formattedDate = date
                    .toLocaleDateString('en-GB', {
                        day: 'numeric',
                        month: 'short',
                        year: 'numeric'
                    })
                    .replace(/ /g, '-');
                this.context.commit('addDataMenuViewCollectWeek', new RecordMenuViews(new Date(formattedDate), weeks));
            } else {
                const weeksViewsByDay = menuWeekViews.filter(m => m.createdAt.getDate() == numberOfDay).length;
                const date = new Date();
                date.setFullYear(date.getFullYear(), date.getMonth(), numberOfDay);
                this.context.commit(
                    'addDataMenuViewCollectWeek',
                    new RecordMenuViews(new Date(dateToString(date)), weeksViewsByDay)
                );
            }
        }
    }

    @Action({ rawError: true })
    async getMenusViewsCollectMonthly(payload: { menuId: string }) {
        this.context.commit('setRecordMenuViewsCollectMonth', []);
        //Limite es el mes actual
        const date = new Date();
        const mesActual = date.getMonth();
        const diaActual = date.getDate();

        date.setFullYear(date.getFullYear(), mesActual, diaActual);
        date.setHours(0, 0, 0);

        const menuViewsMonthly = await this.kpiService.getMenusViewsByType(
            payload.menuId,
            mesActual,
            DataRange.MONTHLY,
            ViewType.COLLECT
        );

        //Recorre array y lo pasa al constructor RecordMenuViews
        for (let dayOfMonth = 1; dayOfMonth <= DataRange.MONTHLY + 1; dayOfMonth++) {
            const monthViews = menuViewsMonthly.filter(m => m.createdAt.getDate() == dayOfMonth).length;

            const date = new Date();
            date.setFullYear(date.getFullYear(), date.getMonth() - 1, dayOfMonth);
            date.setHours(0, 0, 0);
            const formattedDate = date
                .toLocaleDateString('en-GB', {
                    day: 'numeric',
                    month: 'short',
                    year: 'numeric'
                })
                .replace(/ /g, '-');
            this.context.commit(
                'addDataMenuViewCollectMonth',
                new RecordMenuViews(new Date(formattedDate), monthViews)
            );
        }
    }

    @Action({ rawError: true })
    async getMenusViewsCollectPerDay(payload: { menuId: string }) {
        this.context.commit('setRecordMenuViewsCollectPerDay', []);
        //Limite es el mes actual
        const date = new Date();
        const mesActual = date.getMonth();
        const diaActual = date.getDate();

        date.setFullYear(date.getFullYear(), mesActual, diaActual);
        date.setHours(0, 0, 0);

        const menuViewsPerDay = await this.kpiService.getMenusViewsByType(
            payload.menuId,
            mesActual,
            DataRange.DAILY,
            ViewType.COLLECT
        );
        for (let numberMonth = date.getDate() - 1; numberMonth <= date.getDate() - 1; numberMonth++) {
            const menuViews = menuViewsPerDay.filter(m => m.createdAt.getDate() == numberMonth).length;
            const date = new Date();
            date.setFullYear(date.getFullYear(), date.getMonth(), numberMonth);
            this.context.commit(
                'addDataMenuViewCollectPerDay',
                new RecordMenuViews(new Date(dateToString(date)), menuViews)
            );
        }
    }
}
