export const isAlpha = (text?: string): boolean => {
    //limpiar caracteres raros
    //Acepta mayùsculas, minusculas,espacios, No acepta nùmeros
    const expRegCategory = /^([a-zA-Z0-9()ÑñÁáÉéÍíÓóÚúÜü\s]|\p{sc=Hani})+$/gu;

    if (!text || !expRegCategory.exec(text)) {
        console.log('text no alpha', text);
        return false;
    }
    return true;
};

export const isNumericText = (text?: string): boolean => {
    const expRegDNumbers = /^[0-9]+(,[0-9]+)?$/;

    if (!text || !expRegDNumbers.exec(String(text))) {
        console.log('text no numeric', text);
        return false;
    }

    return true;
};

export const dateToString = (date:Date): string =>{
 return date
     .toLocaleDateString('en-GB', {
         day: 'numeric',
         month: 'short',
         year: 'numeric'
     })
     .replace(/ /g, '-');
}