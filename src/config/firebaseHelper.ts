import store from '@/store';
import { auth } from '@/config/firebase';

export const startAuthStateChanged = () => {
    auth.onAuthStateChanged(async authUser => {
        if (authUser) {
            await store.dispatch('LoginModule/incomingUser', authUser.uid);
        }
    });
};
