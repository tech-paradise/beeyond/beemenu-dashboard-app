import { firestore } from 'firebase/app';
import { Restaurant } from '@/model/Restaurant';
import { OrderDish } from '@/model/order/OrderDish';
import { IngredientOrderDish } from '@/model/order/IngredientOrderDish';
import { Order, StatusOrder, TypeOrder } from '@/model/order/Order';
import { User } from '@/model/User';
import { Menu } from '@/model/menu/Menu';
import { Category } from '@/model/menu/Category';
import { LangMenu } from '@/model/menu/LangMenu';
import { Dish } from '@/model/menu/Dish';
import { LangText } from '@/model/LangText';
import { Price } from '@/model/menu/Price';
import { Ingredient } from '@/model/menu/Ingredient';
import { ExtraIngredient } from '@/model/menu/ExtraIngredient';
import { Reviews } from '@/model/menu/Reviews';
import { View } from '@/model/kpi/View';
import { DeliveryMenu } from '@/model/delivery/DeliveryMenu';
import { DEFAULT_COST_SHIPPING } from '@/model/delivery/CostShipping';
import { DEFAULT_PHONES } from '@/model/delivery/Phones';
import { DaysOfWeek, DEFAULT_SCHEDULE, ScheduleFromFB } from '@/model/delivery/Schedule';
import { DEFAULT_PAYMENT } from '@/model/delivery/Payment';
import { DEFAULT_LOCATION } from '@/model/delivery/Location';
import SocialNetworks from '@/model/socialNetworks/SocialNetworks';
import { Subcategory } from '@/model/menu/Subcategory';
import { ExtraIngredientOrderDish } from '@/model/order/ExtraIngredientOrderDish';
import { GatewayOrder } from '@/model/order/GatewayOrder';

export const newTimestamp = function(): firestore.Timestamp {
    return firestore.Timestamp.fromDate(new Date());
};

export const serializeViews = (snapshot: firestore.DocumentSnapshot): View => {
    const data = snapshot.data();

    return new View(snapshot.id, (data?.createdAt as firestore.Timestamp).toDate(), data?.viewFrom || '');
};

export const serializeCategory = (snapshot: firestore.DocumentSnapshot): Category => {
    // console.log('en serialize category');
    const data = snapshot.data();

    return new Category(
        snapshot.id,
        data?.active || false,
        data?.name || '',
        data?.showsAtHome || false,
        data?.showsAtHomePriority || 0,
        data?.icon || '',
        data?.order || 0,
        data?.lastUpdated || newTimestamp()
    );
};

export const serializeSubCategory = (snapshot: firestore.DocumentSnapshot): Subcategory => {
    const data = snapshot.data();
    return new Subcategory(snapshot.id, data?.active || false, data?.icon || '', data?.name || '', data?.order || 0);
};

export const serializeLangMenu = (snapshot: firestore.DocumentSnapshot): LangMenu => {
    // console.log('en serialize category');
    const data = snapshot.data();

    return new LangMenu(
        snapshot.id,
        data?.name || '',
        data?.currency || '',
        data?.icon || '',
        data?.language || '',
        data?.order || 0,
        data?.lastUpdated || newTimestamp(),
        data?.change || 0
    );
};

export const serializeLangText = (snapshot: firestore.DocumentSnapshot): LangText => {
    // console.log('en serialize category');
    const data = snapshot.data();

    return new LangText(snapshot.id, data?.language || '', data?.text || '');
};

export const serializePrice = (snapshot: firestore.DocumentSnapshot): Price => {
    // console.log('en serialize category');
    const data = snapshot.data();

    return new Price(snapshot.id, data?.ammount || 0, data?.currency || '');
};

export const serializeDish = (snapshot: firestore.DocumentSnapshot): Dish => {
    // console.log('en serialize category');
    const data = snapshot.data();

    return new Dish(
        snapshot.id,
        data?.active || false,
        data?.categoryId || '',
        data?.chefRecommended || false,
        data?.chefRecommendedPriority || 0,
        data?.delivery || false,
        data?.description || '',
        data?.menuId || '',
        data?.mostSelled || false,
        data?.mostSelledPriority || 0,
        data?.name || '',
        data?.order || 0,
        data?.photos || '',
        data?.lastUpdated || newTimestamp(),
        data?.subCategoryId || '',
        data?.video || ''
    );
};

export const serializeMenu = (snapshot: firestore.DocumentSnapshot): Menu => {
    const data = snapshot.data();

    return new Menu(
        snapshot.id,
        data?.background || '',
        data?.buttonTextColor || '',
        data?.currency || '',
        data?.description || '',
        data?.logo || '',
        data?.mainColor || '',
        data?.name || '',
        data?.restaurantId || '',
        data?.showChefRecommended || false,
        data?.showMostSelled || false,
        data?.showReviews || false,
        data?.lastUpdated || newTimestamp(),
        data?.active || false,
        data?.useLogin || false
    );
};

export const serializeDelivery = (snapshot: firestore.DocumentSnapshot): DeliveryMenu => {
    const data = snapshot.data();

    const schedule = DEFAULT_SCHEDULE;
    if (data?.schedule) {
        for (const day in data.schedule) {
            const dayAux = day as DaysOfWeek;
            const daySchedule = (data.schedule as ScheduleFromFB)[dayAux];
            schedule[dayAux] = {
                initHour: daySchedule.initHour.toDate(),
                initDay: daySchedule.initDay as DaysOfWeek,
                endHour: daySchedule.endHour.toDate(),
                endDay: daySchedule.endDay as DaysOfWeek,
                open: daySchedule.open
            };
        }
    }

    return new DeliveryMenu(
        snapshot.id,
        data?.address || '',
        data?.costShipping || DEFAULT_COST_SHIPPING,
        data?.menuId || '',
        data?.payment || DEFAULT_PAYMENT,
        data?.phones || DEFAULT_PHONES,
        data?.restaurantId || '',
        schedule,
        data?.imageDelivery || '',
        data?.collect || false,
        data?.shipping || false,
        data?.descriptionDeliveryDefault || '',
        data?.location || DEFAULT_LOCATION,
        data?.active || false,
        data?.onlinePayment || false
    );
};

export const serializeRestaurant = (snapshot: firestore.DocumentSnapshot): Restaurant => {
    const data = snapshot.data();
    return new Restaurant(
        snapshot.id,
        data?.name || '',
        data?.recceiveOrders || '',
        data?.menusRef || [],
        data?.lastUpdated || newTimestamp()
    );
};

export const serializeIngredientOrderDish = (snapshot: firestore.DocumentSnapshot): IngredientOrderDish => {
    const data = snapshot.data();

    return new IngredientOrderDish(
        snapshot.id,
        data?.id || '',
        data?.localizedName || '',
        data?.name || '',
        data?.selected || ''
    );
};

export const serializeExtraIngredientOrderDish = (snapshot: firestore.DocumentSnapshot): ExtraIngredientOrderDish => {
    const data = snapshot.data();

    return new ExtraIngredientOrderDish(
        snapshot.id,
        data?.ingredientId || '',
        data?.localizedName || '',
        data?.price || '',
        data?.selected || false
    );
};

export const serializeIngredient = (snapshot: firestore.DocumentSnapshot): Ingredient => {
    const data = snapshot.data();

    return new Ingredient(
        snapshot.id,
        data?.maxAmount || 0,
        data?.name || '',
        data?.productId || '',
        data?.required || false
    );
};

export const serializeExtraIngredient = (snapshot: firestore.DocumentSnapshot): ExtraIngredient => {
    const data = snapshot.data();

    return new ExtraIngredient(snapshot.id, data?.name || '');
};

export const serializeOrderDish = (snapshot: firestore.DocumentSnapshot): OrderDish => {
    const data = snapshot.data();

    return new OrderDish(
        snapshot.id,
        data?.details || '',
        data?.id || '',
        data?.localizedName || '',
        data?.name || '',
        data?.price || 0,
        data?.quantity || 0,
        data?.type || '',
        data?.category || '',
        data?.subcategory || ''
    );
};

export const serializeOrder = (snapshot: firestore.DocumentSnapshot): Order => {
    const data = snapshot.data();
    const detailsPayment = data?.detailsPayment
        ? new GatewayOrder(
              data.detailsPayment.orderIdGateway,
              data.detailsPayment.status,
              data.detailsPayment.type,
              data.detailsPayment.authBank,
              data.detailsPayment.fee,
              data.detailsPayment.amount,
              data.detailsPayment.totalDeposit,
              data.detailsPayment.cardBank,
              data.detailsPayment.cardBrand,
              data.detailsPayment.cardName
          )
        : GatewayOrder.empty();

    return new Order(
        snapshot.id,
        data?.createdAt.toDate(),
        data?.currency || '',
        data?.cashInfo || '',
        data?.location || '',
        data?.menuId || '',
        data?.name || '',
        data?.phone || '',
        data?.address || {},
        data?.methodPayment || '',
        data?.readableId || '',
        data?.restaurantId || '',
        data?.status || StatusOrder.PENDING,
        data?.subtotal || 0,
        data?.total || 0,
        data?.type || TypeOrder.LOCAL,
        data?.updatedAt || Date.now(),
        data?.userId || '',
        detailsPayment
    );
};

export const serializeReview = (snapshot: firestore.DocumentSnapshot): Reviews => {
    const data = snapshot.data();

    console.log('en serialize review', data);

    return new Reviews(
        snapshot.id,
        data?.approved || false,
        data?.content || '',
        data?.createdAt || '',
        data?.language || '',
        data?.menuId || '',
        data?.name || '',
        data?.positive || false,
        data?.productId || ''
    );
};

export const serializeUser = (snapshot: firestore.DocumentSnapshot): User => {
    const data = snapshot.data();

    return new User(snapshot.id, data?.name || '', data?.lastName || '', data?.restaurantsRef || [], data?.rols || []);
};

export const serializeSocialNetworks = (snapshot: firestore.DocumentSnapshot): SocialNetworks => {
    const data = snapshot.data();

    return new SocialNetworks(
        snapshot.id,
        data?.facebook || '',
        data?.instagram || '',
        data?.menuId || '',
        data?.restaurantId || ''
    );
};

export const mainSerialize = (snapshot: firestore.DocumentSnapshot) => {
    const data = snapshot.data();

    if (data?.isUser) {
        return serializeUser(snapshot);
    }

    if (data?.isRestaurant) {
        return serializeRestaurant(snapshot);
    }

    if (data?.isMenu) {
        return serializeMenu(snapshot);
    }

    if (data?.isDelivery) {
        return serializeDelivery(snapshot);
    }

    return new User('', '', '', [], []);
};
