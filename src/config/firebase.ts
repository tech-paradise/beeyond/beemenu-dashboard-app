import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';
import 'firebase/storage';

const fbConfig = {
    apiKey: process.env.VUE_APP_FB_API_KEY,
    authDomain: process.env.VUE_APP_FB_AUTH_DOMAINS,
    databaseURL: process.env.VUE_APP_FB_DATABASE_URL,
    projectId: process.env.VUE_APP_FB_PROJECT_ID,
    storageBucket: process.env.VUE_APP_FB_STORAGE_BUCKET,
    messagingSenderId: process.env.VUE_APP_FB_MESSANGING_SENDER_ID,
    appId: process.env.VUE_APP_FB_API_ID
};

const fb = firebase.initializeApp(fbConfig);

export const db = fb.firestore();
export const auth = fb.auth();
export const storage = fb.storage();
export const GProvider = new firebase.auth.GoogleAuthProvider();
