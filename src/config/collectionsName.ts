export const USER = 'users';
export const RESTAURANTS = 'restaurants';

export const ORDERS = 'orders';
export const ORDER_DISH = 'items';
export const ORDER_DISH_INGREDIENT = 'ingredients';
export const ORDER_DISH_EXTRAINGREDIENT = 'extraIngredients';

export const MENUS = 'menus';
export const MENU_LANG = 'localizations';

export const CATEGORIES = 'categories';
export const CATEGORY_LANGS_NAME = 'localizedNames';

export const SUBCATEGORIES = 'subcategories';
export const SUBCATEGORY_LANGS_NAME = 'localizedNames';

export const DISH = 'products';
export const DISH_PRICE = 'prices';
export const DISH_DESCRIPTIONS = 'localizedDescriptions';
export const DISH_NAMES = 'localizedNames';
export const DISH_INGREDIENTS = 'ingredients';
export const DISH_INGRE_NAMES = 'localizedNames';

export const INGREDIENTS = 'ingredients';
export const INGREDIENT_NAMES = 'localizedNames';
export const INGREDIENT_EXTRA = 'extraIngredients';
export const INGREDIENT_EXTRA_NAMES = 'localizedNames';
export const INGREDIENT_EXTRA_PRICES = 'prices';

export const DELIVERIES = 'deliveries';
export const DELIVERIES_DESCRIPTIONS = 'descriptionDelivery';
export const DELIVERIES_ESTIMATES = 'descriptionEstimates';

export const SOCIALNETWORKS = 'socialNetworks';

export const REVIEWS = 'reviews';
export const VIEW = 'views';
export const SHIPPINGVIEW = 'shippingviews';

export const COLLECTVIEWS = 'collectviews';

export const DELIVERY = 'deliveries';

export const STORAGE_BACKGROUND = 'fondos/';
export const STORAGE_LOGO = 'logos/';
export const STORAGE_CATEGORY = 'categorias/';
export const STORAGE_SUBCATEGORY = 'subcategorias/';
export const STORAGE_LANG_MENU = 'banderas/';
export const STORAGE_PRODUCTS = 'productos/';
export const STORAGE_DELIVERY = 'logos/';
