import { RouteConfig } from 'vue-router';
import Home from '@/views/dashboard/Home.vue';
import OrdersRestaurants from '@/views/dashboard/orders/OrdersRestaurants.vue';
import Orders from '@/views/dashboard/orders/Orders.vue';
import MenusRestaurants from '@/views/dashboard/menus/MenusRestaurants.vue';
import Menu from '@/views/dashboard/menus/Menu.vue';
import OrderItem from '@/views/dashboard/orders/OrderItem.vue';
import Kpi from '@/views/dashboard/kpis/Kpi.vue';
import KpiMenu from '@/views/dashboard/kpis/KpiMenu.vue';
import Review from '@/views/dashboard/menus/Review.vue';
import Helpdesk from '@/views/dashboard/support/Helpdesk.vue';
import DeliveryRestaurant from '@/views/dashboard/delivery/DeliveryRestaurants.vue';
import RestaurantInfo from '@/views/dashboard/delivery/RestaurantInfo.vue';
import store from '@/store';
import { User } from '@/model/User';
import { Restaurant } from '@/model/Restaurant';
import firebase from 'firebase';
import { Menu as MenuModel } from '@/model/menu/Menu';
import Timestamp = firebase.firestore.Timestamp;
import DishesMenu from '@/views/dashboard/dishes/DishesMenu.vue';
import Dishes from '@/views/dashboard/dishes/Dishes.vue';

const metaMenu = (icon: string): { icon: string; menu: boolean } => {
    return {
        menu: true,
        icon
    };
};

export const mainMenuRoutes: RouteConfig[] = [
    {
        path: '/',
        name: 'INICIO',
        component: Home,
        meta: metaMenu('home')
    },
    {
        path: 'restaurant/menu',
        name: 'MIS NEGOCIOS',
        component: MenusRestaurants,
        meta: metaMenu('menus')
    },
    {
        path: 'restaurant/menu/dishes',
        name: 'PRODUCTOS',
        component: Dishes,
        meta: metaMenu('dishes')
    },
    {
        path: 'restaurant/order',
        name: 'PEDIDOS',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: OrdersRestaurants,
        meta: metaMenu('orders')
    },
    {
        path: 'restaurant/menu/kpi',
        name: 'INDICADORES',
        component: Kpi,
        meta: metaMenu('kpi')
    },
    {
        path: 'restaurant/:id/order',
        name: 'ordersRestaurants',
        component: Orders
    },
    {
        path: 'menu/:id',
        name: 'menu',
        component: Menu
    },
    {
        path: 'OrderItem/:id',
        name: 'OrderItem',
        component: OrderItem
    },
    {
        path: 'menu/:id/review',
        name: 'Review',
        component: Review
    },
    {
        path: 'restaurant/:restaurantId/menu/:menuId/dishes',
        name: 'DishesMenu',
        component: DishesMenu,
        props: route => {
            const user: User = store.state.LoginModule.user;
            const restaurant: Restaurant | undefined = user.restaurants.find(
                res => res.id === route.params.restaurantId
            );
            if (user.rols.includes('BEE')) {
                return {
                    restaurant: new Restaurant('', '', false, [], Timestamp.now())
                };
            }
            return { restaurant: restaurant };
        }
    },
    {
        path: 'restaurant/:restaurantId/menu/:menuId/kpi',
        name: 'KpiMenu',
        component: KpiMenu,
        props: route => {
            const user: User = store.state.LoginModule.user;
            const restaurant: Restaurant | undefined = user.restaurants.find(
                res => res.id === route.params.restaurantId
            );
            if (user.rols.includes('BEE')) {
                return {
                    restaurant: new Restaurant('', '', false, [], Timestamp.now()),
                    menu: new MenuModel(
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        false,
                        false,
                        false,
                        Timestamp.now(),
                        false,
                        false
                    )
                };
            }
            return { restaurant: restaurant, menu: restaurant?.menus.find(m => m.id == route.params.menuId) };
        }
    },
    {
        path: 'support/helpdesk',
        name: 'ASISTENCIA',
        component: Helpdesk,
        meta: metaMenu('help')
    },
    {
        path: 'delivery/restaurants/',
        name: 'DELIVERY',
        component: DeliveryRestaurant,
        meta: metaMenu('delivery')
    },
    {
        path: 'menu/:id/RestaurantInfo',
        name: 'RESTAURANTINFO',
        component: RestaurantInfo
    }
];
