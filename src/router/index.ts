import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import { mainMenuRoutes } from '@/router/mainMenuRoutes';
import Login from '@/views/Login.vue';
import Dashboard from '@/views/Dashboard.vue';
import store from '@/store';
import { User } from '@/model/User';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
    {
        path: '/admin',
        component: Dashboard,
        children: mainMenuRoutes,
        meta: {
            requiresAuth: true
        }
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/',
        redirect: '/admin'
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach(async (to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        const states = store.state;
        const user: User = states.LoginModule.user;
        //console.log('en before', to.fullPath);
        //console.log('en before', JSON.stringify(user));

        if (!user.id) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });
            await store.dispatch('LoginModule/incomingError', 'Inicia sesión');
        } else if (!(user.rols.includes('ADMIN') || user.rols.includes('SUPERADMIN') || user.rols.includes('BEE'))) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });
            await store.dispatch('LoginModule/incomingError', 'Usuario sin los permisos necesarios');
        } else {
            next();
        }
    } else {
        next(); // make sure to always call next()!
    }
});
export default router;
