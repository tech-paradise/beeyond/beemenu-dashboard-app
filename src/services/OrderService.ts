import { db } from '@/config/firebase';
import { ORDERS } from '@/config/collectionsName';
import { newTimestamp } from '@/config/serializables';

export class OrderService {
    async updateStatusOrder(orderId: string, status: string): Promise<void> {
        if (status == 'COMPLETED' || status == 'PENDING' || status == 'CANCEL') {
            return db
                .collection(ORDERS)
                .doc(orderId)
                .update({
                    status: status,
                    updatedAt: newTimestamp()
                });
        }
    }
}
