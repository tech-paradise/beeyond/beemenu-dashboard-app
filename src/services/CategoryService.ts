import { db, storage } from '@/config/firebase';
import {
    CATEGORIES,
    CATEGORY_LANGS_NAME,
    MENUS,
    STORAGE_CATEGORY,
    SUBCATEGORIES,
    SUBCATEGORY_LANGS_NAME
} from '@/config/collectionsName';
import { Category } from '@/model/menu/Category';
import { LangText } from '@/model/LangText';
import firebase from 'firebase';
import { newTimestamp } from '@/config/serializables';
import DocumentData = firebase.firestore.DocumentData;
import CollectionReference = firebase.firestore.CollectionReference;
import DocumentReference = firebase.firestore.DocumentReference;

export class CategoryService {
    async updtaeMenuCategory(menuId: string, category: Category, langsCategory?: LangText[], fileIconCategory?: File) {
        const ref = db
            .collection(MENUS)
            .doc(menuId)
            .collection(CATEGORIES)
            .doc(category.id);
        await ref.update({
            name: category.name,
            active: category.active,
            showsAtHome: category.showsAtHome,
            lastUpdated: newTimestamp(),
            order: Number(category.order),
            showsAtHomePriority: Number(category.showsAtHomePriority)
        });

        if (langsCategory) {
            this.langsUpdate(ref.collection(CATEGORY_LANGS_NAME), langsCategory);
        }

        if (fileIconCategory) {
            await this.updateCategoryIcon(ref, category, fileIconCategory);
        }
    }

    async createMenuCategory(menuId: string, category: Category, langsCategory?: LangText[], fileIconCategory?: File) {
        const ref = await db
            .collection(MENUS)
            .doc(menuId)
            .collection(CATEGORIES)
            .add({
                name: category.name,
                active: category.active,
                showsAtHome: category.showsAtHome,
                lastUpdated: newTimestamp(),
                order: category.order,
                showsAtHomePriority: category.showsAtHomePriority
            });

        if (langsCategory) {
            this.langsUpdate(ref.collection(CATEGORY_LANGS_NAME), langsCategory);
        }

        if (fileIconCategory) {
            await this.updateCategoryIcon(ref, category, fileIconCategory);
        }
    }

    async deleteMenuCategory(menuId: string, category: Category) {
        if (category.icon) {
            await storage.ref(category.icon).delete();
        }
        const document = await db
            .collection(MENUS)
            .doc(menuId)
            .collection(CATEGORIES)
            .doc(category.id);

        //eliminar categorias con sus child collection elements
        const localizedNamesCategoryDB = await document.collection(CATEGORY_LANGS_NAME).get();
        localizedNamesCategoryDB.forEach(localizedNames => {
            localizedNames.ref.delete();
        });

        //eliminar sucategorias con sus child elements
        const subcategoriesCategoryDB = await document.collection(SUBCATEGORIES).get();
        subcategoriesCategoryDB.forEach(async subcategories => {
            const aux = await subcategories.ref.collection(SUBCATEGORY_LANGS_NAME).get();
            aux.forEach(localizedNames => {
                localizedNames.ref.delete();
            });
            subcategories.ref.delete();
        });

        const subcategoryLocalizedDB = await document.get();
        await subcategoryLocalizedDB.ref.delete();
        document.delete();
    }

    private langsUpdate(ref: CollectionReference<DocumentData>, langsCategory: LangText[]) {
        langsCategory.forEach(lan => ref.doc(lan.id).set({ language: lan.language, text: lan.text }));
    }

    private async updateCategoryIcon(ref: DocumentReference<DocumentData>, category: Category, file: File) {
        if (file.size && file.name) {
            const timestamp = Date.now();
            const extension = file.name.split('.').pop();
            const filename = file.name.split('.').shift();
            const newFullPathFile = STORAGE_CATEGORY + filename + timestamp + '.' + extension;

            const data = await storage.ref(newFullPathFile).put(file);
            await ref.update({
                icon: data.metadata.fullPath
            });
            if (category.icon) {
                await storage.ref(category.icon).delete();
            }
            // console.log('oli', data.metadata.fullPath);
        }
    }
}
