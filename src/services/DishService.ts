import { newTimestamp, serializeDish } from '@/config/serializables';
import { Dish, Dish as DishModel } from '@/model/menu/Dish';
import { LangText } from '@/model/LangText';
import { Price } from '@/model/menu/Price';
import { Restaurant } from '@/model/Restaurant';
import { db, storage } from '@/config/firebase';
import {
    DISH,
    DISH_DESCRIPTIONS,
    DISH_NAMES,
    DISH_PRICE,
    INGREDIENT_EXTRA,
    INGREDIENT_EXTRA_NAMES,
    INGREDIENT_EXTRA_PRICES,
    INGREDIENT_NAMES,
    INGREDIENTS,
    MENUS,
    REVIEWS,
    STORAGE_PRODUCTS,
    VIEW
} from '@/config/collectionsName';
import firebase from 'firebase';
import { Ingredient } from '@/model/menu/Ingredient';
import { ExtraIngredient } from '@/model/menu/ExtraIngredient';
import DocumentData = firebase.firestore.DocumentData;
import CollectionReference = firebase.firestore.CollectionReference;
import DocumentReference = firebase.firestore.DocumentReference;

enum OP {
    CREATE,
    UPDATE
}

export class DishService {
    async createDish(
        menuId: string,
        dish: DishModel,
        fileDish?: File[],
        langsName?: LangText[],
        langsDes?: LangText[],
        prices?: Price[],
        restaurant?: Restaurant
    ) {
        return this.dishSave(OP.CREATE, menuId, dish, fileDish, langsName, langsDes, prices, restaurant);
    }
    async updateDish(
        menuId: string,
        dish: DishModel,
        fileDish?: File[],
        langsName?: LangText[],
        langsDes?: LangText[],
        prices?: Price[],
        restaurant?: Restaurant
    ) {
        return this.dishSave(OP.UPDATE, menuId, dish, fileDish, langsName, langsDes, prices, restaurant);
    }

    async deleteDish(menuId: string, dish: DishModel) {
        if (dish.photos) {
            dish.photos.forEach(async photo => {
                await storage.ref(photo).delete();
            });
        }

        const dishDb = await db
            .collection(MENUS)
            .doc(menuId)
            .collection(DISH)
            .doc(dish.id);

        await this.deleteIngredients(dish);

        const localizedNamesDishDB = await dishDb.collection(DISH_NAMES).get();
        localizedNamesDishDB.forEach(localizedNames => {
            localizedNames.ref.delete();
        });
        const localizedDescriptionsDishDB = await dishDb.collection(DISH_DESCRIPTIONS).get();
        localizedDescriptionsDishDB.forEach(localizedDescriptions => {
            localizedDescriptions.ref.delete();
        });

        const PricesDishDB = await dishDb.collection(DISH_PRICE).get();
        PricesDishDB.forEach(prices => {
            prices.ref.delete();
        });

        const reviewsDB = await dishDb.collection(REVIEWS).get();
        reviewsDB.forEach(reviews => {
            reviews.ref.delete();
        });

        const viewsDB = await dishDb.collection(VIEW).get();
        viewsDB.forEach(views => {
            views.ref.delete();
        });
        dishDb.delete();
    }
    async deleteIngredients(dish: DishModel) {
        const collectionRef = await db.collection(INGREDIENTS);

        const ingredients = collectionRef.where('productId', '==', dish.id).get();
        ingredients.then(aux => {
            aux.docs.forEach(aux => {
                aux.ref.delete();
            });
        });
    }

    async dishSave(
        op: OP,
        menuId: string,
        dish: DishModel,
        fileDish?: File[],
        langsName?: LangText[],
        langsDes?: LangText[],
        prices?: Price[],
        restaurant?: Restaurant
    ) {
        const ref = db
            .collection(MENUS)
            .doc(menuId)
            .collection(DISH);

        const dataDish = {
            active: dish.active,
            categoryId: dish.categoryId,
            chefRecommended: dish.chefRecommended,
            chefRecommendedPriority: dish.chefRecommendedPriority,
            description: dish.description,
            menuId: op === OP.UPDATE ? dish.menuId : menuId,
            mostSelled: dish.mostSelled,
            mostSelledPriority: dish.mostSelledPriority,
            name: dish.name,
            order: dish.order,
            lastUpdated: newTimestamp(),
            subCategoryId: dish.subCategoryId,
            video: dish.video
        };

        let newRef: DocumentReference<DocumentData>;

        switch (op) {
            case OP.CREATE:
                newRef = await ref.add(dataDish);
                break;
            case OP.UPDATE:
                newRef = ref.doc(dish.id);
                await newRef.update(dataDish);
        }

        if (langsName) {
            this.langsUpdate(newRef.collection(DISH_NAMES), langsName);
        }

        if (langsDes) {
            this.langsUpdate(newRef.collection(DISH_DESCRIPTIONS), langsDes);
        }

        if (prices) {
            prices.forEach(price =>
                newRef
                    .collection(DISH_PRICE)
                    .doc(price.id)
                    .set({ ammount: price.amount, currency: price.currency })
            );
        }

        if (fileDish && restaurant) {
            await this.updateDishPhoto(newRef, dish, fileDish, restaurant);
        }
    }

    async createDishIngredient(menuId: string, ingre: Ingredient, langsName?: LangText[]) {
        return this.ingredientSave(OP.CREATE, menuId, ingre, langsName);
    }

    async updateDishIngredient(menuId: string, ingre: Ingredient, langsName?: LangText[]) {
        return this.ingredientSave(OP.UPDATE, menuId, ingre, langsName);
    }

    async ingredientSave(op: OP, menuId: string, ingre: Ingredient, langsName?: LangText[]) {
        const ref = db
            .collection(MENUS)
            .doc(menuId)
            .collection(INGREDIENTS);

        let newRef: DocumentReference<DocumentData>;

        switch (op) {
            case OP.CREATE:
                newRef = await ref.add({
                    name: ingre.name,
                    maxAmount: ingre.maxAmount,
                    productId: ingre.productId,
                    required: ingre.required
                });
                break;
            case OP.UPDATE:
                newRef = ref.doc(ingre.id);
                await newRef.update({
                    name: ingre.name,
                    maxAmount: ingre.maxAmount,
                    productId: ingre.productId,
                    required: ingre.required
                });
        }

        if (langsName) {
            this.langsUpdate(newRef.collection(DISH_NAMES), langsName);
        }
    }

    async createExtraIngredient(
        menuId: string,
        ingredientId: string,
        extraIngredient: ExtraIngredient,
        langsName?: LangText[],
        prices?: Price[]
    ) {
        return this.ExtraIngredientSave(OP.CREATE, menuId, ingredientId, extraIngredient, langsName, prices);
    }

    async updateExtraIngredient(
        menuId: string,
        ingredientId: string,
        extraIngredient: ExtraIngredient,
        langsName?: LangText[],
        prices?: Price[]
    ) {
        return this.ExtraIngredientSave(OP.UPDATE, menuId, ingredientId, extraIngredient, langsName, prices);
    }

    async ExtraIngredientSave(
        op: OP,
        menuId: string,
        ingredientId: string,
        extraIngredient: ExtraIngredient,
        langsName?: LangText[],
        prices?: Price[]
    ) {
        const ref = db
            .collection(MENUS)
            .doc(menuId)
            .collection(INGREDIENTS)
            .doc(ingredientId)
            .collection(INGREDIENT_EXTRA);

        const dataExtraIngredient = {
            name: extraIngredient.name
        };

        let newRef: DocumentReference<DocumentData>;

        switch (op) {
            case OP.CREATE:
                newRef = await ref.add(dataExtraIngredient);
                break;
            case OP.UPDATE:
                newRef = ref.doc(extraIngredient.id);
                await newRef.update(dataExtraIngredient);
                break;
        }

        if (langsName) {
            this.langsUpdate(newRef.collection(INGREDIENT_EXTRA_NAMES), langsName);
        }

        if (prices) {
            this.pricesUpdate(newRef.collection(INGREDIENT_EXTRA_PRICES), prices);
        }
    }

    async deleteDishIngredient(menuId: string, ingre: Ingredient) {
        const document = db
            .collection(MENUS)
            .doc(menuId)
            .collection(INGREDIENTS)
            .doc(ingre.id);

        const ingredientLangsDB = await document.collection(INGREDIENT_NAMES).get();

        ingredientLangsDB.forEach(async lang => {
            await lang.ref.delete();
        });

        const ingredientDB = await document.get();

        await ingredientDB.ref.delete();
    }

    async deleteExtra(menuId: string, ingredientId: string, extraIngredientId: string) {
        const document = db
            .collection(MENUS)
            .doc(menuId)
            .collection(INGREDIENTS)
            .doc(ingredientId)
            .collection(INGREDIENT_EXTRA)
            .doc(extraIngredientId);

        const extraLangsDB = await document.collection(INGREDIENT_EXTRA_NAMES).get();

        extraLangsDB.forEach(async lang => {
            await lang.ref.delete();
        });

        const extraPricesDB = await document.collection(INGREDIENT_EXTRA_PRICES).get();

        extraPricesDB.forEach(async price => {
            await price.ref.delete();
        });

        const extraDB = await document.get();

        await extraDB.ref.delete();
    }

    private langsUpdate(ref: CollectionReference<DocumentData>, langsCategory: LangText[]) {
        langsCategory.forEach(lan => ref.doc(lan.id).set({ language: lan.language, text: lan.text }));
    }

    private pricesUpdate(ref: CollectionReference<DocumentData>, prices: Price[]) {
        prices.forEach(price => ref.doc(price.id).set({ ammount: price.amount, currency: price.currency }));
    }

    private async updateDishPhoto(
        ref: DocumentReference<DocumentData>,
        dish: DishModel,
        files: File[],
        restaurant: Restaurant
    ) {
        const filesArr = await Promise.all(
            files
                .filter(file => file.name !== '')
                .map(async file => {
                    if (file.name) {
                        if (file.size) {
                            const timestamp = Date.now();
                            const extension = file.name.split('.').pop();
                            const filename = file.name.split('.').shift();
                            const newFullPathFile =
                                STORAGE_PRODUCTS + restaurant.name + '/' + filename + timestamp + '.' + extension;
                            const data = await storage.ref(newFullPathFile).put(file);
                            return data.metadata.fullPath;
                        } else {
                            return file.name;
                        }
                    }
                })
        );
        if (dish.photos.length > 0) {
            const diffArr = dish.photos.filter(photo => !filesArr.includes(photo));
            if (diffArr.length > 0) {
                diffArr.forEach(async photo => {
                    if (photo != '') {
                        await storage.ref(photo).delete();
                    }
                });
            }
            await ref.update({
                photos: firebase.firestore.FieldValue.arrayRemove(...dish.photos)
            });
        }
        if (filesArr.length > 0) {
            await ref.update({
                photos: firebase.firestore.FieldValue.arrayUnion(...filesArr)
            });
        }
    }

    async getDishesFromMenu(menuId: string): Promise<DishModel[]> {
        const ref = db
            .collection(MENUS)
            .doc(menuId)
            .collection(DISH);

        const snapsDish = await ref.get();
        const dishes: Dish[] = [];

        snapsDish.forEach(snap => dishes.push(serializeDish(snap)));
        console.log('dishes', dishes);
        return dishes;
    }
}
