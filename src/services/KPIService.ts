import { db } from '@/config/firebase';
import { COLLECTVIEWS, DELIVERY, DISH, MENUS, SHIPPINGVIEW, VIEW } from '@/config/collectionsName';
import { Dish } from '@/model/menu/Dish';
import { serializeViews } from '@/config/serializables';
import { ProductsViews, View, ViewType } from '@/model/kpi/View';
import { DataRange } from '@/model/DataRange';
import { TypeOrder } from '@/model/order/Order';

export class KPIService {
    //Obtiene cantidad de Views por producto de acuerdo a un rango de tiempo (mes,semana,dia)
    async getProductsViews(
        menuId: string,
        dishes: Dish[],
        days: DataRange,
        typeDish: TypeOrder
    ): Promise<ProductsViews[]> {
        const ref = db
            .collection(MENUS)
            .doc(menuId)
            .collection(DISH);
        const startDate = new Date();
        const endDate = new Date();
        const currentYear = endDate.getFullYear();

        switch (days) {
            case DataRange.MONTHLY:
                startDate.setFullYear(currentYear, startDate.getMonth() - 1, 1);
                startDate.setHours(0, 0, 0);
                endDate.setFullYear(currentYear, endDate.getMonth(), 0);
                endDate.setHours(23, 59, 59);
                break;
            default:
                startDate.setFullYear(currentYear, startDate.getMonth(), startDate.getDate() - days);
                startDate.setHours(0, 0, 0);
                endDate.setFullYear(currentYear, startDate.getMonth(), endDate.getDate());
                endDate.setHours(0, 0, 0);
                break;
        }
        const viewsData: ProductsViews[] = [];
        for (const dish of dishes) {
            const snapsViews = await ref
                .doc(dish.id)
                .collection(VIEW)
                .where('createdAt', '>=', startDate)
                .where('createdAt', '<=', endDate)
                .get();

            const viewes: View[] = [];
            snapsViews.forEach(doc => viewes.push(serializeViews(doc)));
            const dateView: View[] = viewes.filter(order => order.viewFrom == typeDish);
            const date: Date[] = [];
            for (const view of dateView) {
                date.push(new Date(view.createdAt));
            }

            viewsData.push({ value: dateView.length, name: dish.name, photos: dish.photos, createdAt: date[0] });
        }
        return viewsData.sort((a, b) => b.value - a.value);
    }

    //Obtener views por typo de servicio (LOCAL,DELIVERY,MENSUAL)
    async getMenusViewsByType(menuId: string, month: number, days: DataRange, viewType: ViewType): Promise<View[]> {
        let ref: firebase.firestore.CollectionReference<firebase.firestore.DocumentData>;

        switch (viewType) {
            case ViewType.LOCAL:
                ref = db
                    .collection(MENUS)
                    .doc(menuId)
                    .collection(VIEW);
                break;
            case ViewType.DELIVERY:
                ref = db
                    .collection(DELIVERY)
                    .doc(menuId)
                    .collection(SHIPPINGVIEW);
                break;
            default:
                ref = db
                    .collection(DELIVERY)
                    .doc(menuId)
                    .collection(COLLECTVIEWS);
        }

        const startDate = new Date();
        const endDate = new Date();
        const currentYear = startDate.getFullYear();
        switch (days) {
            case DataRange.MONTHLY:
                startDate.setFullYear(currentYear, startDate.getMonth() - 1, 1);
                startDate.setHours(0, 0, 0);
                endDate.setMonth(endDate.getMonth(), 0);
                endDate.setHours(23, 59, 59);
                if (month === 0) {
                    startDate.setFullYear(currentYear - 1, 11, 1);
                }
                break;
            default:
                startDate.setFullYear(currentYear, month, startDate.getDate() - days);
                startDate.setHours(0, 0, 0);
                endDate.setFullYear(currentYear, startDate.getMonth(), endDate.getDate());
                endDate.setHours(23, 59, 59);
                break;
        }
        const snapsViews = await ref
            .where('createdAt', '>=', startDate)
            .where('createdAt', '<=', endDate)
            .get();
        const menusViews: View[] = [];
        snapsViews.forEach(doc => menusViews.push(serializeViews(doc)));
        return menusViews;
    }

    //+ADDED

    //Obtener views por typo de servicio (LOCAL,DELIVERY,MENSUAL)
    async getMenusViewsType(menuId: string, month: number): Promise<View[]> {
       const ref = db
            .collection(MENUS)
            .doc(menuId)
            .collection(VIEW);

        const startDate = new Date();
        const endDate = new Date();
        const currentYear = startDate.getFullYear();

        startDate.setFullYear(currentYear, startDate.getMonth() - 1, 0);
        startDate.setHours(0, 0, 0);
        endDate.setFullYear(currentYear, endDate.getMonth(), endDate.getDate()-1);
        endDate.setHours(23, 59, 59);

        if (month === 0) {
            startDate.setFullYear(currentYear - 1, 10, 1);
        }
        const snapsViews = await ref
            .where('createdAt', '>=', startDate)
            .where('createdAt', '<=', endDate)
            .get();
        console.log('dates',startDate+'-'+endDate)
        const menusViews: View[] = [];
        snapsViews.forEach(doc => menusViews.push(serializeViews(doc)));
        return menusViews;
    }
}
