import { newTimestamp } from '@/config/serializables';
import { LangMenu } from '@/model/menu/LangMenu';
import { db, storage } from '@/config/firebase';
import { MENU_LANG, MENUS, STORAGE_LANG_MENU } from '@/config/collectionsName';
import firebase from 'firebase';
import DocumentData = firebase.firestore.DocumentData;
import DocumentReference = firebase.firestore.DocumentReference;

export class LangMenuService {
    async deleteLanguage(menuId: string, lang: LangMenu) {
        const ref = db.collection(MENUS).doc(menuId);
        //const langCat = await ref.collection(`${CATEGORIES}/*/${CATEGORY_LANGS_NAME}/${lang.id}`).get();
        //console.log('del lang cat', langCat);
        // TODO delete langs for products and ingredients
        //langCat.forEach(snap => snap.ref.delete());
        await ref
            .collection(MENU_LANG)
            .doc(lang.id)
            .delete();
    }

    async createLanguage(menuId: string, lang: LangMenu, fileIconLang?: File) {
        const ref = await db
            .collection(MENUS)
            .doc(menuId)
            .collection(MENU_LANG)
            .add({
                currency: lang.currency,
                language: lang.language,
                name: lang.name,
                order: lang.order,
                lastUpdated: newTimestamp(),
                change: lang.change
            });

        if (fileIconLang) {
            await this.updateFile(ref, lang, fileIconLang);
        }
    }

    async updateLanguage(menuId: string, lang: LangMenu, fileIconLang?: File) {
        const ref = db
            .collection(MENUS)
            .doc(menuId)
            .collection(MENU_LANG)
            .doc(lang.id);
        await ref.update({
            currency: lang.currency,
            language: lang.language,
            name: lang.name,
            order: lang.order,
            lastUpdated: newTimestamp(),
            change: lang.change
        });

        if (fileIconLang) {
            await this.updateFile(ref, lang, fileIconLang);
        }
    }

    async updateFile(ref: DocumentReference<DocumentData>, lang: LangMenu, file: File) {
        if (file.size && file.name) {
            const timestamp = Date.now();
            const extension = file.name.split('.').pop();
            const filename = file.name.split('.').shift();
            const newFullPathFile = STORAGE_LANG_MENU + filename + timestamp + '.' + extension;

            const data = await storage.ref(newFullPathFile).put(file);
            await ref.update({
                icon: data.metadata.fullPath
            });
            if (lang.icon) {
                await storage.ref(lang.icon).delete();
            }
        }
    }
}
