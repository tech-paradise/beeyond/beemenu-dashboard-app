import { db } from '@/config/firebase';
import { SOCIALNETWORKS } from '@/config/collectionsName';
import SocialNetworks from '@/model/socialNetworks/SocialNetworks';
import firebase from 'firebase';

export class SocialNetworksService {
    async updateSocialNetworks(socialNetwork: SocialNetworks): Promise<void> {
        const ref = await db.collection(SOCIALNETWORKS).doc(socialNetwork.id);
        await ref.update({
            facebook: socialNetwork.facebook,
            instagram: socialNetwork.instagram
        });
    }
}
