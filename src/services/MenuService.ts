import { newTimestamp } from '@/config/serializables';
import { db, storage } from '@/config/firebase';
import { DISH, MENUS, STORAGE_BACKGROUND, STORAGE_LOGO } from '@/config/collectionsName';
import { Menu } from '@/model/menu/Menu';

export class MenuService {
    async updateMenu(partialMenu: {
        id: string;
        name: string;
        description: string;
        buttonTextColor: string;
        mainColor: string;
        showChefRecommended: boolean;
        showMostSelled: boolean;
        showReviews: boolean;
        useLogin: boolean
    }): Promise<void> {
        await db
            .collection(MENUS)
            .doc(partialMenu.id)
            .update({
                buttonTextColor: partialMenu.buttonTextColor,
                description: partialMenu.description,
                mainColor: partialMenu.mainColor,
                name: partialMenu.name,
                showChefRecommended: partialMenu.showChefRecommended,
                showMostSelled: partialMenu.showMostSelled,
                showReviews: partialMenu.showReviews,
                useLogin: partialMenu.useLogin,
                lastUpdated: newTimestamp()
            });
    }

    async updateMenuFile(menu: Menu, file: File, path: string) {
        if (file.size && file.name) {
            const menuDBRef = db.collection(MENUS).doc(menu.id);
            const timestamp = Date.now();
            const extension = file.name.split('.').pop();
            const filename = file.name.split('.').shift();
            const newFullPathFile = path + filename + timestamp + '.' + extension;

            if (path === STORAGE_BACKGROUND) {
                const data = await storage.ref(newFullPathFile).put(file);
                await menuDBRef.update({
                    background: data.metadata.fullPath,
                    lastUpdated: newTimestamp()
                });
                try {
                    const spaceRef = await storage.ref(menu.background);
                    spaceRef.delete();
                    console.log('oli', data.metadata.fullPath);
                } catch (e) {
                    const error = e as Error;
                    if (error.message.includes('null')) {
                        console.log('message', error.message);
                    }
                }
                return;
            }

            if (path === STORAGE_LOGO) {
                const data = await storage.ref(newFullPathFile).put(file);
                await menuDBRef.update({
                    lastUpdated: newTimestamp(),
                    logo: data.metadata.fullPath
                });
                try {
                    const spaceRef = await storage.ref(menu.logo);
                    spaceRef.delete();
                    console.log('oli', data.metadata.fullPath);
                } catch (e) {
                    const error = e as Error;
                    if (error.message.includes('null')) {
                        console.log('message', error.message);
                    }
                }
                return;
            }
            throw new Error('Path Incorrecto');
        }
        throw new Error('Debes seleccionar un archivo y debe tener un nombre');
    }

    async updateStatusTypeService(
        partialMenuStatus: { dishId: string; delivery: boolean },
        menuId: string
    ): Promise<void> {
        return await db
            .collection(MENUS)
            .doc(menuId)
            .collection(DISH)
            .doc(partialMenuStatus.dishId)
            .update({
                delivery: partialMenuStatus.delivery,
                lastUpdated: new Date()
            });
    }
}
