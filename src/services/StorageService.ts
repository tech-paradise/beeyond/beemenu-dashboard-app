import { storage } from '@/config/firebase';

export async function getFile(filePath: string | null): Promise<string> {
    if (filePath == null || filePath == '') {
        return '';
    }

    try {
        return await storage
            .ref('/')
            .child(filePath)
            .getDownloadURL();
    } catch (error) {
        console.log('error', error);
        return '';
    }
}
