import { db, storage } from '@/config/firebase';
import {
    CATEGORIES,
    CATEGORY_LANGS_NAME,
    DELIVERIES,
    MENUS,
    STORAGE_DELIVERY,
    STORAGE_SUBCATEGORY,
    SUBCATEGORIES,
    SUBCATEGORY_LANGS_NAME
} from '@/config/collectionsName';
import { LangText } from '@/model/LangText';
import firebase from 'firebase';
import { Subcategory } from '@/model/menu/Subcategory';
import DocumentData = firebase.firestore.DocumentData;
import CollectionReference = firebase.firestore.CollectionReference;
import DocumentReference = firebase.firestore.DocumentReference;
import { DeliveryMenu } from '@/model/delivery/DeliveryMenu';
import { newTimestamp } from '@/config/serializables';

export class SubcategoryService {
    async updtaeMenuSubCategory(
        menuId: string,
        categoryId: string,
        subcategory: Subcategory,
        langsSubCategory?: LangText[],
        fileIconSubCategory?: File
    ) {
        /** console.log(
            'menuid' + menuId,
            'categoryId' + categoryId,
            'subcategory' + subcategory,
            'langs' + langsSubCategory,
            'files' + fileIconSubCategory
        );
        **/
        const ref = db
            .collection(MENUS)
            .doc(menuId)
            .collection(CATEGORIES)
            .doc(categoryId)
            .collection(SUBCATEGORIES)
            .doc(subcategory.id);
        await ref.update({
            active: subcategory.active,
            name: subcategory.name,
            order: subcategory.order
        });
        if (langsSubCategory) {
            this.langsUpdate(ref.collection(CATEGORY_LANGS_NAME), langsSubCategory);
        }
        if (fileIconSubCategory) {
            await this.updateSubCategoryIcon(ref, subcategory, fileIconSubCategory);
        }
    }

    async updateMenuSubCategoryBatch(menuId: string, categoryId: string, subcategory: Subcategory[]) {
        const obj = Object.values(subcategory);
        const filterr = obj.filter(x => x.id);
        //  console.log('filter', filterr);

        const batch = db.batch();

        for (const property in filterr) {
            /**
            console.log(
                `${property}: ${filterr[property].id} ,
                 ${filterr[property].active},
                 ${filterr[property].name},
                 ${filterr[property].order}`
            );
           **/
            const id = filterr[property].id;
            const active = filterr[property].active;
            const name = filterr[property].name;
            const order = filterr[property].order;

            const ref = db
                .collection(MENUS)
                .doc(menuId)
                .collection(CATEGORIES)
                .doc(categoryId)
                .collection(SUBCATEGORIES)
                .doc(id);
            await batch.update(ref, {
                active: active,
                name: name,
                order: order
            });
        }
        // console.log('menuid' + menuId, 'categoryId' + categoryId, 'subcategorys' + Object.values(subcategory));
        return batch.commit().then(function() {
            //   console.log('hecho');
        });
    }

    async updateSubLanguages(newlanguage: LangText, menuId: string, categoryId: string, subcategoryId: string) {
        const ref = db
            .collection(MENUS)
            .doc(menuId)
            .collection(CATEGORIES)
            .doc(categoryId)
            .collection(SUBCATEGORIES)
            .doc(subcategoryId)
            .collection(SUBCATEGORY_LANGS_NAME)
            .doc(newlanguage.id);
        await ref.set({
            language: newlanguage.language,
            text: newlanguage.text
        });
    }
    async createMenuSubCategory(
        menuId: string,
        categoryId: string,
        subcategory: Subcategory,
        langsSubCategory?: LangText[],
        fileIconSubCategory?: File
    ) {
        /**console.log(
            'menuid' + menuId,
            'categoryId' + categoryId,
            'subcategory' + subcategory,
            'langs' + langsSubCategory,
            'files' + fileIconSubCategory,
            'hello'
        );
         **/
        const ref = await db
            .collection(MENUS)
            .doc(menuId)
            .collection(CATEGORIES)
            .doc(categoryId)
            .collection(SUBCATEGORIES)
            .add({
                active: subcategory.active,
                name: subcategory.name,
                order: subcategory.order
            });

        if (langsSubCategory) {
            this.langsUpdate(ref.collection(CATEGORY_LANGS_NAME), langsSubCategory);
        }

        if (fileIconSubCategory) {
            await this.updateSubCategoryIcon(ref, subcategory, fileIconSubCategory);
        }
    }

    async deleteMenuSubCategory(menuId: string, categoryId: string, subcategory: Subcategory) {
        if (subcategory.icon) {
            await storage.ref(subcategory.icon).delete();
        }

        const document = await db
            .collection(MENUS)
            .doc(menuId)
            .collection(CATEGORIES)
            .doc(categoryId)
            .collection(SUBCATEGORIES)
            .doc(subcategory.id);

        const localizedNamesSubcategoryDB = await document.collection('localizedNames').get();

        localizedNamesSubcategoryDB.forEach(async localizedNames =>{
            await localizedNames.ref.delete();
        });

        const subcategoryLocalizedDB = await document.get();
        await subcategoryLocalizedDB.ref.delete();
    }

    private langsUpdate(ref: CollectionReference<DocumentData>, langsSubCategory: LangText[]) {
        langsSubCategory.forEach(lan => ref.doc(lan.id).set({ language: lan.language, text: lan.text }));
    }

    private async updateSubCategoryIcon(ref: DocumentReference<DocumentData>, subcategory: Subcategory, file: File) {
        if (file.size && file.name) {
            const timestamp = Date.now();
            const extension = file.name.split('.').pop();
            const filename = file.name.split('.').shift();
            const newFullPathFile = STORAGE_SUBCATEGORY + filename + timestamp + '.' + extension;

            const data = await storage.ref(newFullPathFile).put(file);
            await ref.update({
                icon: data.metadata.fullPath
            });
            if (subcategory.icon) {
                try {
                    await storage.ref(subcategory.icon).delete();
                } catch (e) {
                    const error = e as Error;
                    if (error.message.includes('null')) {
                        console.log('message', error.message);
                    }
                }
            }
        }
    }

    async updateSubCategoryIconFile(menuId: string, categoryId: string, subcategory: Subcategory, file: File) {
        if (file.size && file.name) {
            const ref = db
                .collection(MENUS)
                .doc(menuId)
                .collection(CATEGORIES)
                .doc(categoryId)
                .collection(SUBCATEGORIES)
                .doc(subcategory.id);

            const timestamp = Date.now();
            const extension = file.name.split('.').pop();
            const filename = file.name.split('.').shift();
            const newFullPathFile = STORAGE_SUBCATEGORY + filename + timestamp + '.' + extension;

            if (newFullPathFile.length != null || undefined) {
                const data = await storage.ref(newFullPathFile).put(file);
                await ref.update({
                    icon: data.metadata.fullPath
                });
                if (subcategory.icon) {
                    try {
                        await storage.ref(subcategory.icon).delete();
                    } catch (e) {
                        const error = e as Error;
                        if (error.message.includes('null')) {
                            console.log('message', error.message);
                        }
                    }
                }
                return;
            }
            throw new Error('Path Incorrecto');
        }
        throw new Error('Debes seleccionar un archivo y debe tener un nombre');
    }
    async updateSubcategoryActive(
        partialSubcategoryActive: {
            active: boolean;
        },
        subcategoryId: string,
        categoryId: string,
        menuId: string
    ): Promise<void> {
        return await db
            .collection(MENUS)
            .doc(menuId)
            .collection(CATEGORIES)
            .doc(categoryId)
            .collection(SUBCATEGORIES)
            .doc(subcategoryId)
            .update({
                active: partialSubcategoryActive.active
            });
    }
}
