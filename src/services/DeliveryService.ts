import { newTimestamp } from '@/config/serializables';
import { db, storage } from '@/config/firebase';
import { DELIVERIES, DELIVERIES_DESCRIPTIONS, DELIVERIES_ESTIMATES, STORAGE_DELIVERY } from '@/config/collectionsName';
import { DaysOfWeek, Schedule, ScheduleFromFB } from '@/model/delivery/Schedule';
import { DeliveryMenu } from '@/model/delivery/DeliveryMenu';
import { SocialNetworksService } from '@/services/SocialNetworksService';
import SocialNetworks from '@/model/socialNetworks/SocialNetworks';
import firebase from 'firebase';
import { LangText } from '@/model/LangText';
import Timestamp = firebase.firestore.Timestamp;

export class DeliveryService {
    async updateDelivery(
        delivery: DeliveryMenu,
        descriptionDelivery: LangText[],
        socialNetworks: SocialNetworks,
        descriptionEstimates: LangText[]
    ): Promise<void> {
        const socialNetworkService = new SocialNetworksService();
        const ref = await db.collection(DELIVERIES).doc(delivery.id);
        await ref.update({
            address: delivery.address,
            collect: delivery.collect,
            costShipping: delivery.costShipping,
            descriptionDeliveryDefault: delivery.descriptionDeliveryDefault,
            location: delivery.location,
            payment: delivery.payment,
            phones: delivery.phones,
            shipping: delivery.shipping,
            active: delivery.active
        });

        descriptionDelivery.forEach(description => {
            ref.collection(DELIVERIES_DESCRIPTIONS)
                .doc(description.id)
                .set({ language: description.language, text: description.text }, { merge: true });
        });

        descriptionEstimates.forEach(estimate => {
            ref.collection(DELIVERIES_ESTIMATES)
                .doc(estimate.id)
                .set({ language: estimate.language, text: estimate.text }, { merge: true });
        });

        socialNetworkService.updateSocialNetworks(socialNetworks);
    }

    async updateDeliverySchedule(deliveryId: string, schedule: Schedule): Promise<void> {
        const scheduleFire: ScheduleFromFB = {} as ScheduleFromFB;

        for (const day in schedule) {
            const dayAux = day as DaysOfWeek;
            const daySchedule = schedule[dayAux];
            scheduleFire[dayAux] = {
                initHour: Timestamp.fromDate(daySchedule.initHour),
                initDay: daySchedule.initDay as DaysOfWeek,
                endHour: Timestamp.fromDate(daySchedule.endHour),
                endDay: daySchedule.endDay as DaysOfWeek,
                open: daySchedule.open
            };
        }

        await db
            .collection(DELIVERIES)
            .doc(deliveryId)
            .update({
                schedule: scheduleFire
            });
    }

    async updateDeliveryFile(delivery: DeliveryMenu, file: File, path: string) {
        if (file.size && file.name) {
            const deliveryDBRef = db.collection(DELIVERIES).doc(delivery.id);
            const timestamp = Date.now();
            const extension = file.name.split('.').pop();
            const filename = file.name.split('.').shift();
            const newFullPathFile = path + filename + timestamp + '.' + extension;
            if (path === STORAGE_DELIVERY) {
                const data = await storage.ref(newFullPathFile).put(file);
                await deliveryDBRef.update({
                    lastUpdated: newTimestamp(),
                    imageDelivery: data.metadata.fullPath
                });
                if (delivery.imageDelivery) {
                    try {
                        await storage.ref(delivery.imageDelivery).delete();
                    } catch (e) {
                        const error = e as Error;
                        if (error.message.includes('null')) {
                            console.log('message', error.message);
                        }
                    }
                }

                return;
            }
            throw new Error('Path Incorrecto');
        }
        throw new Error('Debes seleccionar un archivo y debe tener un nombre');
    }
}
