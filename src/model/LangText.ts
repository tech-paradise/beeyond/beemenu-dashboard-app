export class LangText {
    constructor(public id: string, public language: string, public text: string) {}
}

export const DEFAULT_LANG_TEXT = new LangText('', '', '');
