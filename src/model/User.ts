import { Restaurant } from '@/model/Restaurant';

export class User {
    constructor(
        public id: string,
        public name: string,
        public lastName: string,
        public restaurants: Restaurant[],
        public rols: Array<'BEE' | 'ADMIN' | 'SUPERADMIN' | 'COMMENSAL'>
    ) {}
}
