export class IngredientOrderDish {
    constructor(
        public id: string,
        public ingredientId: string,
        public localizedName: string,
        public name: string,
        public selected: boolean
    ) {}
}
