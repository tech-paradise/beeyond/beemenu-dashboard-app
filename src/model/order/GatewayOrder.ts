export enum PaymentStatus {

    // CONEKTA STATUS
    PENDING_PAYMENT = 'PENDING_PAYMENT',
    DECLINED = 'DECLINED',
    EXPIRED = 'EXPIRED',
    PAID = 'PAID',
    REFUNDED = 'REFUNDED',
    PARTIALLY_REFUNDED = 'PARTIALLY_REFUNDED',
    CHARGED_BACK = 'CHARGED_BACK',
    PRE_AUTHORIZED = 'PRE_AUTHORIZED',
    VOIDED = 'VOIDED',

    // STRIPE STATUS
    REQUIRES_PAYMENT_METHOD = 'REQUIRES_PAYMENT_METHOD',
    REQUIRES_CONFIRMATION = 'REQUIRES_CONFIRMATION',
    REQUIRES_ACTION = 'REQUIRES_ACTION',
    PROCESSING = 'PROCESSING',
    REQUIRES_CAPTURE = 'REQUIRES_CAPTURE',
    CANCELED = 'CANCELED',
    SUCCEEDED = 'SUCCEEDED',
}
export enum TypePayment {
    CONEKTA = 'CONEKTA',
    CASH = 'CASH',
    ND = 'ND',
    STRIPE = 'STRIPE',
}

export class GatewayOrder {
    constructor(
        readonly orderGatewayId: string,
        readonly status: PaymentStatus,
        readonly type: TypePayment,
        readonly authCode: string,
        readonly fee: number,
        readonly amount: number,
        readonly totalDeposit: number,
        readonly cardBank: string,
        readonly cardBrand: string,
        readonly cardName: string
    ) {}

    static empty(): GatewayOrder {
        return new GatewayOrder('', PaymentStatus.PENDING_PAYMENT, TypePayment.ND, '', 0, 0, 0, '', '', '');
    }
}
