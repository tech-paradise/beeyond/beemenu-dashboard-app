export class ExtraIngredientOrderDish {
    constructor(
        public id: string,
        public ingredientId: string,
        public localizedName: string,
        public price: number,
        public selected: boolean
    ) {}
}
