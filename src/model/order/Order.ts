import { addressFB } from '@/model/delivery/address';
import { GatewayOrder } from '@/model/order/GatewayOrder';

export enum TypeOrder {
    LOCAL = 'LOCAL',
    DELIVERY = 'DELIVERY',
    COLLECT = 'COLLECT'
}

export enum StatusOrder {
    COMPLETED = 'COMPLETED',
    PENDING = 'PENDING',
    CANCEL = 'CANCEL'
}

export class Order {
    constructor(
        public id: string,
        public createdAt: Date,
        public currency: string,
        public cashInfo: string,
        public location: string,
        public menuId: string,
        public name: string,
        public phone: string,
        public address: addressFB,
        public methodPayment: string,
        public numberOrder: string,
        public restaurantId: string,
        public status: StatusOrder,
        public subtotal: number,
        public total: number,
        public type: TypeOrder,
        public updatedAt: Date,
        public userId: string,
        public detailsPayment?: GatewayOrder,
    ) {}
}
