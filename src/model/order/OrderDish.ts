export enum ItemType {
    PRODUCT = 'PRODUCT',
    COLLECT = 'COLLECT',
    DELIVERY = 'DELIVERY'
}
export class OrderDish {
    constructor(
        public id: string,
        public details: string,
        public productId: string,
        public localizedName: string,
        public name: string,
        public price: string,
        public quantity: number,
        public type: string,
        public category: string,
        public subcategory: string
    ) {}
}
