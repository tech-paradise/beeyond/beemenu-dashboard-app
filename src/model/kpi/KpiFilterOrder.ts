export enum FilterOrder {
    DAY = 'DAY',
    WEEK = 'WEEK',
    MONTH = 'MONTH'
}
