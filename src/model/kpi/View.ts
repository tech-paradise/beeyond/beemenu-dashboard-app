export enum ViewType {
    LOCAL = 'local',
    DELIVERY = 'delivery',
    COLLECT = 'collect'
}
export class View {
    constructor(public id: string, public createdAt: Date, public viewFrom: string) {}
}
export class RecordMenuViews {
    constructor(public date: Date, public view: number) {}
}
export class ProductsViews {
    constructor(public name: string, public value: number, public photos: string[], public createdAt: Date) {}
}
