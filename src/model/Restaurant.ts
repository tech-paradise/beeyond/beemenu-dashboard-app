import { Menu } from '@/model/menu/Menu';
import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;

export class Restaurant {
    constructor(
        public id: string,
        public name: string,
        public receiveOrders: boolean,
        public menus: Menu[],
        public lastUpdated: Timestamp
    ) {}
}
