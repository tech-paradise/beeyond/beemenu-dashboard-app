export default class SocialNetworks {
    constructor(
        public id: string,
        public facebook: string,
        public instagram: string,
        public menuId: string,
        public restaurantId: string
    ) {}
}
