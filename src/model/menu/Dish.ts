import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;

export class Dish {
    constructor(
        public id: string,
        public active: boolean,
        public categoryId: string,
        public chefRecommended: boolean,
        public chefRecommendedPriority: number,
        public delivery: boolean,
        public description: string,
        public menuId: string,
        public mostSelled: boolean,
        public mostSelledPriority: number,
        public name: string,
        public order: number,
        public photos: string[],
        public lastUpdated: Timestamp,
        public subCategoryId: string,
        public video: string
    ) {}
}
