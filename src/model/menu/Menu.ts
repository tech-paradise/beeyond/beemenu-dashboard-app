import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;
export class Menu {
    constructor(
        public id: string,
        public background: string,
        public buttonTextColor: string,
        public currency: string,
        public description: string,
        public logo: string,
        public mainColor: string,
        public name: string,
        public restaurantId: string,
        public showChefRecommended: boolean,
        public showMostSelled: boolean,
        public showReviews: boolean,
        public lastUpdated: Timestamp,
        public active: boolean,
        public useLogin: boolean
    ) {}
}
