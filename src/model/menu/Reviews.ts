import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;

export class Reviews {
    constructor(
        public id: string,
        public approved: boolean,
        public content: string,
        public createdAt: Timestamp,
        public language: string,
        public menuId: string,
        public name: string,
        public positive: boolean,
        public dishId: string
    ) {}
}
