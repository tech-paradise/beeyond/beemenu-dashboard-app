import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;

export class LangMenu {
    public constructor(
        public id: string,
        public name: string,
        public currency: string,
        public icon: string,
        public language: string,
        public order: number,
        public lastUpdated: Timestamp,
        public change: number
    ) {}
}
