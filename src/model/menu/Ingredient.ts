export class Ingredient {
    public constructor(
        public id: string,
        public maxAmount: number,
        public name: string,
        public productId: string,
        public required: boolean
    ) {}
}

export function DEFAULT_INGREDIENT() {
    return new Ingredient('', 0, '', '', false);
}
