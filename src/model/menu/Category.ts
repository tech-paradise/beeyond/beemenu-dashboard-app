import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;
import { newTimestamp } from '@/config/serializables';

export class Category {
    constructor(
        public id: string,
        public active: boolean,
        public name: string,
        public showsAtHome: boolean,
        public showsAtHomePriority: number,
        public icon: string,
        public order: number,
        public lastUpdated: Timestamp
    ) {}
}

export function DEFAULT_CATEGORY(){
    return new Category('',false,'',false,0,'',0,newTimestamp());
}
