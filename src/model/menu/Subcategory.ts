export class Subcategory {
    constructor(
        public id: string,
        public active: boolean,
        public icon: string,
        public name: string,
        public order: number
    ) {}
}
