export class ExtraIngredient {
    public constructor(public id: string, public name: string) {}
}
