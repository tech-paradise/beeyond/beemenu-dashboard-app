import Radius from '@/model/delivery/Radius';

export class CostShipping {
    constructor(public radius1: Radius, public radius2: Radius, public radius3: Radius, public radius4: Radius) {}
}

export const DEFAULT_COST_SHIPPING = new CostShipping(new Radius(0, 0), new Radius(0, 0), new Radius(0, 0), new Radius(0,0));
