export class Location {
    constructor(public lat: number, public lng: number) {}
}

export const DEFAULT_LOCATION = new Location(0, 0);
