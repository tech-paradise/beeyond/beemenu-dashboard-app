import { CostShipping } from '@/model/delivery/CostShipping';
import { Phones } from '@/model/delivery/Phones';
import { Schedule } from '@/model/delivery/Schedule';
import Payment from '@/model/delivery/Payment';
import { Location } from '@/model/delivery/Location';

export class DeliveryMenu {
    constructor(
        public id: string,
        public address: string,
        public costShipping: CostShipping,
        public menuId: string,
        public payment: Payment,
        public phones: Phones,
        public restaurantId: string,
        public schedule: Schedule,
        public imageDelivery: string,
        public collect: boolean,
        public shipping: boolean,
        public descriptionDeliveryDefault: string,
        public location: Location,
        public active: boolean,
        public onlinePayment: boolean
    ) {}
}
