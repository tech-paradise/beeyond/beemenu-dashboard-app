export class OnlinePayment {
    constructor(public active: boolean, public conekta: boolean, public stripe: boolean) {}
}

export const DEFAULT_ONLINE = new OnlinePayment(false, false, false);
export default class Payment {
    constructor(public cash: boolean, public online: OnlinePayment) {}
}

export const DEFAULT_PAYMENT = new Payment(false, DEFAULT_ONLINE);
