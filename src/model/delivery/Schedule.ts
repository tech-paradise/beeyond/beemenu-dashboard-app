import firebase from 'firebase';
import Timestamp = firebase.firestore.Timestamp;

//enum para los dias de la semana
export enum DaysOfWeek {
    LUNES = 'LUNES',
    MARTES = 'MARTES',
    MIERCOLES = 'MIERCOLES',
    JUEVES = 'JUEVES',
    VIERNES = 'VIERNES',
    SABADO = 'SABADO',
    DOMINGO = 'DOMINGO'
}
//Tipeo de Dias y su arreglo de propiedades

export type ScheduleFromFB = {
    [key in DaysOfWeek]: {
        endDay: string;
        endHour: Timestamp;
        initDay: string;
        initHour: Timestamp;
        open: boolean;
    };
};

export type Schedule = {
    [key in DaysOfWeek]: {
        initDay: DaysOfWeek;
        initHour: Date;
        endDay: DaysOfWeek;
        endHour: Date;
        open: boolean;
    };
};

//Inicializar estructura/arreglo de variables para setear datos de Firebase

export const DEFAULT_SCHEDULE: Schedule = {
    DOMINGO: {
        endDay: DaysOfWeek.DOMINGO,
        endHour: new Date(),
        initDay: DaysOfWeek.DOMINGO,
        initHour: new Date(),
        open: false
    },
    LUNES: {
        endDay: DaysOfWeek.LUNES,
        endHour: new Date(),
        initDay: DaysOfWeek.LUNES,
        initHour: new Date(),
        open: false
    },
    MARTES: {
        endDay: DaysOfWeek.MARTES,
        endHour: new Date(),
        initDay: DaysOfWeek.MARTES,
        initHour: new Date(),
        open: false
    },
    MIERCOLES: {
        endDay: DaysOfWeek.MIERCOLES,
        endHour: new Date(),
        initDay: DaysOfWeek.MIERCOLES,
        initHour: new Date(),
        open: false
    },
    JUEVES: {
        endDay: DaysOfWeek.JUEVES,
        endHour: new Date(),
        initDay: DaysOfWeek.JUEVES,
        initHour: new Date(),
        open: false
    },
    VIERNES: {
        endDay: DaysOfWeek.VIERNES,
        endHour: new Date(),
        initDay: DaysOfWeek.VIERNES,
        initHour: new Date(),
        open: false
    },
    SABADO: {
        endDay: DaysOfWeek.SABADO,
        endHour: new Date(),
        initDay: DaysOfWeek.SABADO,
        initHour: new Date(),
        open: false
    }

};
