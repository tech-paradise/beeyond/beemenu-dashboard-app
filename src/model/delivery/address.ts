
export type addressFB = {
    country: string;
    extNumber: string;
    locality: string;
    number: string;
    references: string;
    region: string;
    state: string;
    street: string;
};

