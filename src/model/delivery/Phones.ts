import LocalPhone from '@/model/delivery/LocalPhone';
import Whatsapp from '@/model/delivery/Whatsapp';

export class Phones {
    constructor(public localPhones: LocalPhone, public whatsapp: Whatsapp) {}
}

export const DEFAULT_PHONES = new Phones(new LocalPhone(''), new Whatsapp('', ''));
