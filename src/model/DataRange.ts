export enum DataRange {
    MONTHLY = 30,
    WEEKLY = 7,
    DAILY = 1
}