import router from './router';
import Vue from 'vue';
import './registerServiceWorker';
import App from './App.vue';
import store from './store';
import vuetify from './plugins/vuetify';
import { startAuthStateChanged } from '@/config/firebaseHelper';
import { firestorePlugin } from 'vuefire';
import moment from 'moment';
import VueApexCharts from 'vue-apexcharts';
import Vuelidate from 'vuelidate';
import VueLazyload from 'vue-lazyload';

Vue.config.productionTip = false;
Vue.use(firestorePlugin, moment, VueApexCharts, Vuelidate);
Vue.component('apexchart', VueApexCharts);
Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: 'dist/error.png',
    loading: 'dist/loading.gif',
    attempt: 1
});

startAuthStateChanged();
new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
}).$mount('#app');
